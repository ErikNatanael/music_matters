use fraction::{Fraction, ToPrimitive};
use lilypond_pargen::lp_node::*;
use musical_matter::instrument::*;
use musical_matter::pitch::*;
use musical_matter::time::*;
use musical_matter::*;
use rand::distributions::WeightedIndex;
use rand::prelude::*;

fn main() {
    // SETUP

    let chords = vec![
        EdoChord::from_relative_degrees(EdoPitch::new(7, 12), &[0, 3, 7]),
        EdoChord::from_relative_degrees(EdoPitch::new(2, 12), &[0, 4, 7, 10]),
        EdoChord::from_relative_degrees(EdoPitch::new(7, 12), &[0, 3, 7]),
        EdoChord::from_relative_degrees(EdoPitch::new(5, 12), &[0, 4, 7]),
        EdoChord::from_relative_degrees(EdoPitch::new(10, 12), &[0, 4, 7]),
        EdoChord::from_relative_degrees(EdoPitch::new(5, 12), &[0, 4, 7]),
        EdoChord::from_relative_degrees(EdoPitch::new(7, 12), &[0, 3, 7]),
        EdoChord::from_relative_degrees(EdoPitch::new(2, 12), &[0, 4, 7, 10]),
    ];

    let start_arp = chords.last().unwrap().arpeggiate(2, -1);
    let mut start_arp2 = start_arp.clone();
    start_arp2
        .rotate(1)
        .make_ascending()
        .transpose(Interval::edo(-12, 12));

    let mut right_hand = MusicStream::new();
    let left_hand = MusicStream::new();


    // Höger hand börjar med G-arpeggio neråt
    right_hand.push_stream(arpeggio(
        vec![19, 23, 26, 30, 31, 33],
        5,
        8,
        true,
        5,
        1,
        3,
        0,
    ));
    right_hand.push_event(Event::rest().dur(Fraction::from(1. / 4.)));
    right_hand.push_stream(arpeggio(
        vec![19, 23, 26, 30, 31, 33],
        5,
        8,
        true,
        5,
        1,
        3,
        0,
    ));
    right_hand.push_event(Event::rest().dur(Fraction::from(1. / 4.)));


    let mut temporal_grid = TemporalGrid::from_bars(vec![Bar::new(4, 4)]);
    temporal_grid.bpm = Some(100.);
    let top_voice = Voice::new(vec![right_hand], &temporal_grid);
    let bottom_voice = Voice::new(vec![left_hand], &temporal_grid);

    let mut instrument = Instrument::marimba();
    instrument.voices = vec![top_voice, bottom_voice];

    // Generate the lilypond stuff
    let lp_node = LPNode::sequence_from(instrument.to_lilypond_vec());
    // Put the whole thing in a document. Since we only have this one
    // voice, we can put it in a document as is.
    let lp_document = LPDocument::from_score(lp_node);
    println!("{}", lp_document.stringify());
    lp_document.open_as_pdf(true);
}


fn arpeggio(
    scale: Vec<i32>,
    start_pos: usize,
    num_notes: usize,
    down_first: bool,
    max_down: usize,
    min_down: usize,
    max_up: usize,
    min_up: usize,
) -> MusicStream {
    let indices = loop {
        let mut rng = rand::thread_rng();
        let mut movement = Vec::with_capacity(num_notes);
        while movement.len() < num_notes {
            let num_up = rng.gen::<usize>() % (max_up - min_up) + min_up;
            let num_down = rng.gen::<usize>() % (max_down - min_down) + min_down;
            if down_first {
                movement.extend(vec![-1; num_down].into_iter());
                movement.extend(vec![1; num_up].into_iter());
            } else {
                movement.extend(vec![1; num_up].into_iter());
                movement.extend(vec![-1; num_down].into_iter());
            }
            if movement.len() > num_notes {
                movement.resize(num_notes, 0); // Truncate if too many notes
            }
        }

        let mut indices = Vec::with_capacity(num_notes);
        let mut ptr = start_pos as i32;
        let mut out_of_bounds = false;
        for m in movement {
            indices.push(ptr);
            ptr += m;
            if ptr >= scale.len() as i32 || ptr < 0 {
                out_of_bounds = true;
                break;
            }
        }
        if out_of_bounds {
            continue;
        } else {
            break indices;
        }
    };

    let events = indices
        .into_iter()
        .map(|i| {
            Event::note(Pitch::edo(scale[i as usize], 12)).dur(Fraction::from(1.0 / 16.0))
        })
        .collect();

    MusicStream::from_events(events)
}
