use fraction::Fraction;
use lilypond_pargen::lp_node::*;
use musical_matter::pitch::*;
use musical_matter::time::*;
use musical_matter::*;

// Generate an overtone series and convert it to 12edo pitches and then to lilypond

fn main() {
    // Generate harmonic series
    // The frequency of c (octave 0 in lilypond). Since we only care
    // about relative frequency here, any pitch would have worked, but
    // this is the actual frequency of degree 0 when converting to
    // lilypond.
    let c0 = 130.8127826503;
    let root_freq = EdoPitch::new(-24 , 12).to_freq_pitch(c0);
    let harmonic_series: Vec<FreqPitch> = (1..=16)
        .map(|i| FreqPitch::new(root_freq.frequency() * i as f32))
        .collect();
    let edo_pitches: Vec<EdoPitch> = harmonic_series
        .into_iter()
        .map(|freq_pitch| freq_pitch.to_edo_pitch(12, c0))
        .collect();

    // Create one rhythmic value per pitch depending on the octave of that pitch
    let beats: Vec<Beat> = edo_pitches
        .iter()
        .map(|edo_pitch| {
            let octave = (edo_pitch.octave() + 3).max(0);
            Fraction::new(1_u64, 2_u64.pow(octave as u32))
        })
        .collect();
    let rhythm_sequence = RhythmSequence::new(beats);

    // Create pitch sequence
    let pitches = edo_pitches
        .into_iter()
        .map(|edo_pitch| Some(Pitch::Edo(edo_pitch)))
        .collect();
    let pitch_sequence = PitchSequence::new(pitches);

    let music_stream = MusicStream::from_rhythm_and_pitch_seqs(rhythm_sequence, pitch_sequence);

    let temporal_grid = TemporalGrid::from_constant_bar_length(Bar::new(4, 4));

    let voice = Voice {
        music_streams: vec![music_stream],
        temporal_grid,
    };

    // Generate the lilypond stuff
    let lp_node = voice.to_lilypond();
    // Put the whole thing in a document. Since we only have this one
    // voice, we can put it in a document as is.
    let lp_document = LPDocument::from_score(lp_node);
    println!("{}", lp_document.stringify());
    lp_document.open_as_pdf(false);
}
