use fraction::Fraction;
use lilypond_pargen::lp_node::*;
use musical_matter::instrument::*;
use musical_matter::pitch::*;
use musical_matter::time::*;
use musical_matter::*;
use rand::prelude::*;

fn main() {
    let music_stream1 = MusicStream::from_rhythm_and_pitch_seqs(
        RhythmSequence::new(vec![Fraction::new(1_u64, 8_u64)]),
        PitchSequence::from_edo_degrees(
            (0..16)
                .map(|_| (rand::random::<u32>() % 24) as i32 )
                .collect(),
            12,
        ),
    );

    let music_stream2 = MusicStream::from_rhythm_and_pitch_seqs(
        RhythmSequence::new(vec![Fraction::new(1_u64, 16_u64)]),
        PitchSequence::from_edo_degrees(
            (0..32)
                .map(|_| (rand::random::<u32>() % 24) as i32)
                .collect(),
            12,
        ),
    );

    let music_stream3 = MusicStream::from_rhythm_and_pitch_seqs(
        RhythmSequence::new(vec![Fraction::new(3_u64, 16_u64)]),
        PitchSequence::from_edo_degrees(
            (0..24)
                .map(|_| (rand::random::<u32>() % 24) as i32)
                .collect(),
            12,
        ),
    );

    let temporal_grid = TemporalGrid::from_bars(vec![Bar::new(4, 4)]);

    let mut voice = Voice::new(vec![music_stream1], &temporal_grid);
    voice.insert_music_stream_last(music_stream2); // Insert after music_stream1
    voice.push_music_stream(music_stream3); // Insert at the start of the voice

    let (top_voice, bottom_voice) = voice.split_at_pitch(Pitch::edo(12, 12));

    let mut instrument = Instrument::marimba();
    instrument.voices = vec![top_voice, bottom_voice];

    // Generate the lilypond stuff
    let lp_node = LPNode::sequence_from(instrument.to_lilypond_vec());
    // Put the whole thing in a document.
    let lp_document = LPDocument::from_score(lp_node);
    lp_document.open_as_pdf(true);
}
