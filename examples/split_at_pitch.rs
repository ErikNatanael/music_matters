use fraction::Fraction;
use lilypond_pargen::lp_node::*;
use musical_matter::instrument::*;
use musical_matter::pitch::*;
use musical_matter::time::*;
use musical_matter::*;
use rand::prelude::*;

fn main() {
    let music_stream = MusicStream::from_rhythm_and_pitch_seqs(
        RhythmSequence::new(vec![Fraction::new(1_u64, 8_u64)]),
        PitchSequence::from_edo_degrees(vec![-12, -5, -1, 3, 7, 10, 12, 13, 14], 12),
    );
    let music_stream = music_stream.transpose(Interval::edo(12, 12));
    let (top_stream, bottom_stream) = music_stream.split_at_pitch(Pitch::edo(14, 12));
    let temporal_grid = TemporalGrid::from_bars(vec![Bar::new(6, 8), Bar::new(3, 8)]);
    let top_voice = Voice::new(vec![top_stream], &temporal_grid);
    let bottom_voice = Voice::new(vec![bottom_stream], &temporal_grid);
    let mut instrument = Instrument::marimba();
    instrument.voices = vec![top_voice, bottom_voice];

    // Generate the lilypond stuff
    let lp_node = LPNode::sequence_from(instrument.to_lilypond_vec());
    // Put the whole thing in a document.
    let lp_document = LPDocument::from_score(lp_node);
    lp_document.open_as_pdf(true);
}
