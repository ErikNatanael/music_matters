use fraction::Fraction;
use musical_matter::pitch::*;
use musical_matter::time::*;
use musical_matter::*;

//Generate a major scale of 4/4 notes in 3/4 bars and convert to Lilypond

fn main() {
    // The long version, typing out every step
    let degrees = vec![0, 2, 4, 5, 7, 9, 11, 12];
    let edo_pitches: Vec<EdoPitch> = degrees
        .into_iter()
        .map(|degree| EdoPitch::new(degree, 12))
        .collect();
    let pitches = edo_pitches
        .into_iter()
        .map(|edo_pitch| Some(Pitch::Edo(edo_pitch)))
        .collect();
    let mut pitch_sequence = PitchSequence::new(pitches);

    // Create one quarter note for each pitch
    let beats: Vec<Beat> = pitch_sequence
        .get_pitches()
        .iter()
        .map(|_| Fraction::new(1_u64, 4_u64))
        .collect();
    let rhythm_sequence = RhythmSequence::new(beats);

    let music_stream = MusicStream::from_rhythm_and_pitch_seqs(rhythm_sequence, pitch_sequence);

    let temporal_grid = TemporalGrid::from_constant_bar_length(Bar::new(3, 4));

    let voice = Voice {
        music_streams: vec![music_stream],
        temporal_grid,
    };

    // Generate the lilypond stuff
    let lp_node = voice.to_lilypond();
    println!("{}", lp_node.stringify());
}
