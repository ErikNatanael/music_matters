# Examples/test programs

- [x] Generate a scale of 4/4 notes in 3/4 bars and convert to Lilypond
- [x] Generate a harmonic series in frequencies, convert them to 12edo and render as a sequence in Lilypond

# Overview

A Part can contain many voices and can be converted into a Lilypond part.

A voice can contain many MusicStreams which map parameters (such as pitch, articulation, dynamics) to durations.

MusicStreams can be aligned to different TemporalGrids (which can be applied to any MusicStream in any part in any voice.

To generate a note in music notation, all of the MusicStream values for a given voice are combined. Then all of the voices in the part are combined to generate chords only where needed.

## PitchSequence

A sequence of pitches separated by rests.

Iterator provides the following functions that can be useful for pitch:
- cycle
- zip
- unzip
- take
- chain

## MusicStream

A music stream is the combination of differen types of sequences of musical material, such as pitch, rhythm, articulation, dynamics etc. that are all aligned so that an index into one sequence and an index into another give values that would relate to the same note in traditional music notation.

The rhythmical values are mandatory.

In addition, several MusicStreams can be aligned (e.g. if some automation doesn't require an automation point for every pitch) by applying the same temporal grid to both (see Voice). That way, the spcific values of all parameters of a Voice can be determined at a specific temporal location both in beats and in seconds.

## Voice

A voice contains one temporal grid and one or more MusicStreams.

Each MusicStream has an offset so that several MusicStreams can be combined in sequence or in parallell.

The Voice coordinates its different parts so that time can be added or removed

## TemporalGrid

The temporal grid covers a large number of parameters. It contains 
a) the bar layout of the section as an Iterator
b) the length of each beat in a bar (optional)
c) tempo related notation signs as a MusicStream (optional)
d) any number of tempo automations (optional)

# Representing musical material

## Tremolo

Tremolo is a collection of musical techniques and sounds.
Temporal distinctions:
- measured tremolo: just a convenient way of writing out repeated pitches
- unmeasured tremolo: a very different sound from writing out exact pitches
- unmeasured tremolo, but with varying speed: 

Pitch distinctions:
- on one pitch - bow/normal tremolo on string instruments, flutter-tounging on wind instruments
- between two pitches - finger tremolo on string instruments, normal tremolo on wind instruments

Technique distinctions:
On the marimba, tremolo can be performed either with lateral strokes or with one hand at a time for different sounds.

For measured tremolo, the notation of it is simply an export/visualisation matter of the underlying written out material. Unmeasured tremolo is a kind of aleatoric notation where the speed is usually not indicated, but it could be.

## Aleatoric notation

An item of aleatoric notation, such as a box, is an event containing other events. It has a duration and can be rendered to musical notation in different ways.

The duration of an aleatoric event can either be fixed (in musical time) or depend on some real world event such as a cue or another player doing something.

## Text

There are many kinds of text: staff text (applies to one instrument), system text (applies to all instruments), [dynamics, articulation, techniques] (apply to one or more notes) and more. Each type of text would be treated differently in export. Some would sometimes be rendered as text and sometimes as symbols (e.g. cresc. or <).

Each type of text requires its own solution.

## Playing techniques

Playing techniques are sometimes rendered as text, sometimes using symbols attached to notes, or a combination of both. 

## Dynamics

Dynamics can be rendered using text, symbols or through other means. Standard music notation has the following 8 degrees:
ppp, pp, p, mp, mf, f, ff, fff

More p:s or f:s can be added. Additionally, transitions to or within dynamics are given using cresc., dim., decresc., <, >, o<, >o, al niente, dal niente.

In traditional music notation, dynamics have a further function of partly defining the timbre and interpretation (phrasing, "feeling" etc) of the music. This could be opt in for other notations.

For electronic sounds it may make more sense to determine an amplitude with more fine grained control over the movement in amplitude.

Dynamics can also be annotated with the speed with which they change e.g. subito, sfz, fz etc. The difference between fz and the articulation > is very subtle. sfz and fz may be considered articulations instead of dynamics. 

### Can any musical event have a dynamic?

Most musical events definitely can. Even combinations of events, such as aleatoric events, can have an overall dynamic in addition to dynamics within e.g. a box.

# Converting to Lilypond
