use std::convert::TryInto;

use nom::{
    branch::alt,
    bytes::complete::tag,
    // see the "streaming/complete" paragraph lower for an explanation of these submodules
    character::complete::{char, digit0, digit1, one_of},
    combinator::opt,
    error::{ErrorKind, ParseError},
    multi::{count, many0, many0_count},
    sequence::delimited,
    Err::Error,
    ErrorConvert,
    IResult,
};

#[derive(Debug, PartialEq)]
pub enum ParseChordSymbolError<I> {
    MyError,
    EllipsisError,
    EdoPitchError,
    InsufficientData,
    Nom(I, ErrorKind),
}

impl<'a> From<(&'a str, ErrorKind)> for ParseChordSymbolError<&'a str> {
    fn from((i, ek): (&'a str, ErrorKind)) -> Self {
        ParseChordSymbolError::Nom(i, ek)
    }
}
impl<I> From<nom::error::Error<I>> for ParseChordSymbolError<I> {
    fn from(value: nom::error::Error<I>) -> Self {
        Self::Nom(value.input, value.code)
    }
}
impl<I> From<nom::Err<nom::error::Error<I>>> for ParseChordSymbolError<I> {
    fn from(value: nom::Err<nom::error::Error<I>>) -> Self {
        match value {
            nom::Err::Incomplete(_) => Self::InsufficientData,
            Error(e) => e.into(),
            nom::Err::Failure(e) => e.into(),
        }
        // Self::Nom(value.input, value.code)
    }
}
impl<'a> From<()> for ParseChordSymbolError<&'a str> {
    fn from(_: ()) -> Self {
        ParseChordSymbolError::EllipsisError
    }
}

impl<I> ParseError<I> for ParseChordSymbolError<I> {
    fn from_error_kind(input: I, kind: ErrorKind) -> Self {
        ParseChordSymbolError::Nom(input, kind)
    }

    fn append(_: I, _: ErrorKind, other: Self) -> Self {
        other
    }
}

use super::{
    edo_n_letter_to_degrees, edo_n_sharp_degrees, EdoChordSemantic, EdoIntervalSemantic, EdoPitch,
    MacroAlteration,
};

// TODO: Proper error
pub fn parse_up_down_chord_symbol(symbol: &str, divisions: u32) -> Result<EdoChordSemantic, ()> {
    match parse_up_down_chord_symbol_nom(symbol, divisions) {
        Ok(chord) => Ok(chord.1),
        Err(_) => Err(()),
    }
}

fn parse_up_down_chord_symbol_nom(
    input: &str,
    edo: u32,
) -> IResult<&str, EdoChordSemantic, ParseChordSymbolError<&str>> {
    // First pitch can be in the form "C" or "vC#"
    // Match N incl 0 occurrences of ^ or v
    let (input, main_pitch_ups) = many0_count(char('^'))(input)?;
    let (input, main_pitch_downs) = many0_count(char('v'))(input)?;
    // Match N incl 0 occurrences of ~
    let (input, main_pitch_mids) = many0_count(char('~'))(input)?;
    // Match a standard pitch name (required) e.g. C#

    let (input, main_pitch_letter) = one_of("ABCDEFG")(input)?;
    let (input, main_pitch_sharpflat) = opt(alt((tag("#"), tag("b"))))(input)?;

    let mut main_pitch_number = edo_n_letter_to_degrees(edo, main_pitch_letter).unwrap();
    main_pitch_number += main_pitch_ups as i32;
    main_pitch_number -= main_pitch_downs as i32;
    if let Some(sharpflat) = main_pitch_sharpflat {
        let sharp = edo_n_sharp_degrees(edo);
        if let Some(sharp) = sharp {
            if sharpflat == "#" {
                main_pitch_number += sharp as i32;
            } else if sharpflat == "b" {
                main_pitch_number -= sharp as i32;
            }
        }
    }

    // Match an interval (N ^ or v followed by m, M, d or a)
    // 5. Context is formed by the default context (M2, M3, P4, P5, M6, m7, M9, P11, M13 as usual) and the macro
    //    alteration right after the root. The macro alteration after the root behaves differently from macro alterations applied to a single pitch:
    // 5a. 'm' e.g. Cm13 turns 3 into minor
    // 5b. 'M' e.g. CM7,v13 turns 7 into major
    // 5c. 'd' e.g. Cd13 turns 3 into minor, 5 into diminished and 7 into diminished
    // 5d. 'a' e.g. Ca9 turns 5 into augmented

    let (input, micro_alt_ups) = many0_count(char('^'))(input)?;
    let (input, micro_alt_downs) = many0_count(char('v'))(input)?;
    let main_micro_alt = micro_alt_ups as i32 - micro_alt_downs as i32;
    let (input, macro_alt_letter) = opt(one_of("mMda"))(input)?;
    let main_macro_alt: MacroAlteration = macro_alt_letter.try_into().unwrap();

    // Match a decimal number which is the highest standard stacked third, e.g. 7, 9, 11, 13 (optional, include 3 and 5 if none)

    let (input, highest_contiguous) = opt(digit1)(input)?;

    // We can now create the basic chord without extra alterations.

    let root = EdoPitch::new(main_pitch_number, edo);
    let mut chord = if let Some(highest_contiguous) = highest_contiguous {
        let highest_contiguous = highest_contiguous.parse::<u32>().unwrap();
        if highest_contiguous % 2 == 0 {
            todo!() // return error, this is invalid
        }
        let mut chord = EdoChordSemantic::from_root(root);
        chord.set_interval(
            EdoIntervalSemantic::from_number(1, edo)
                .map_err(|e| Error(ParseChordSymbolError::from(e)))?,
        ); // All chords have the prime, except if it has a no1
        if highest_contiguous == 5 {
            // same as no3
            chord.set_interval(
                EdoIntervalSemantic::from_number(5, edo)
                    .map_err(|e| Error(ParseChordSymbolError::from(e)))?,
            );
        } else {
            chord.set_interval(
                EdoIntervalSemantic::from_number(3, edo)
                    .map_err(|e| Error(ParseChordSymbolError::from(e)))?,
            );
            chord.set_interval(
                EdoIntervalSemantic::from_number(5, edo)
                    .map_err(|e| Error(ParseChordSymbolError::from(e)))?,
            );
        }

        for i in (7..=highest_contiguous).step_by(2) {
            chord.set_interval(
                EdoIntervalSemantic::from_number(i, edo)
                    .map_err(|e| Error(ParseChordSymbolError::from(e)))?,
            );
        }
        chord
    } else {
        // There was no number specified
        EdoChordSemantic::from_root_major(root)
    };
    chord.apply_main_alterations(main_macro_alt, main_micro_alt);

    // Match any occurence of a comma followed by a number (an "add" interval) or a set of parentheses
    //  containing a modified interval e.g. "(vM7)". If the macro alteration is missing,
    // infer from the chord macro alteration:
    // 6. Alterations to the third e.g. C7(^3) use the same macro alteration (major/minor/mid) as the main chord. C7(^3) has a ^M3, Cm7(^3) has a ^m3, C~7(^3) has a ^~3. Same goes for other pitches.

    let (input, added_intervals) = many0(alt((
        parse_added_interval(edo, main_macro_alt, main_micro_alt),
        parse_altered_interval(edo, main_macro_alt, main_micro_alt),
    )))(input)
    .map_err(|e| Error(ParseChordSymbolError::from(e)))?;
    for i in added_intervals {
        chord.set_interval(i);
    }

    let (input, nos) = many0(alt((tag("no1"), tag("no3"), tag("no5"))))(input)
        .map_err(|e| Error(ParseChordSymbolError::from(e)))?;
    for no in nos {
        match no {
            "no1" => chord.remove_interval(1),
            "no3" => chord.remove_interval(3),
            "no5" => chord.remove_interval(5),
            _ => (),
        }
    }

    // Match any occurrence of no1, no3, no5 at the end

    // sus, sus2, sus4, sus24 are not supported, use the add comma instead.

    //

    // Next comes an optional m (minor), a (augmented) or d (diminished) e.g. "vC#d"

    // Then come the max standard stacking of thirds e.g. "vC#d13" "vC#^m7"

    // Then come parentheses with altered notes inside e.g. "vC#d13(vM3)(^7)"
    Ok((input, chord))
}

fn parse_added_interval(
    edo: u32,
    main_macro_alt: MacroAlteration,
    main_micro_alt: i32,
) -> impl FnMut(&str) -> IResult<&str, EdoIntervalSemantic> {
    move |input| {
        let (input, _) = alt((tag(","), tag("add"), tag(",add")))(input)?;
        let (input, loacl_micro_alt_ups) = many0_count(char('^'))(input)?;
        let (input, local_micro_alt_downs) = many0_count(char('v'))(input)?;
        let local_micro_alt = loacl_micro_alt_ups as i32 - local_micro_alt_downs as i32;
        let (input, local_macro_alteration) = opt(one_of("mMda"))(input)?;
        let local_macro_alt: MacroAlteration = local_macro_alteration.try_into().unwrap();
        let (input, number) = digit1(input)?;
        let number = number.parse().unwrap();
        let mut interval = EdoIntervalSemantic::from_number(number, edo).unwrap();
        interval
            .apply_main_alteration(main_macro_alt, main_micro_alt)
            .unwrap();
        interval.apply_individual_alteration(local_macro_alt, local_micro_alt);
        Ok((input, interval))
    }
}
fn parse_altered_interval(
    edo: u32,
    main_macro_alt: MacroAlteration,
    main_micro_alt: i32,
) -> impl FnMut(&str) -> IResult<&str, EdoIntervalSemantic> {
    move |input| {
        let (input, _) = tag("(")(input)?;
        let (input, local_micro_alt_ups) = many0_count(char('^'))(input)?;
        let (input, local_micro_alt_downs) = many0_count(char('v'))(input)?;
        let local_micro_alt = local_micro_alt_ups as i32 - local_micro_alt_downs as i32;
        let (input, local_macro_alteration) = opt(one_of("mMda"))(input)?;
        let (input, number) = digit1(input)?;
        let number = number.parse().unwrap();
        let (input, _) = tag(")")(input)?;
        let local_macro_alt: MacroAlteration = local_macro_alteration.try_into().unwrap();
        let mut interval = EdoIntervalSemantic::from_number(number, edo).unwrap();
        interval
            .apply_main_alteration(main_macro_alt, main_micro_alt)
            .unwrap();
        interval.apply_individual_alteration(local_macro_alt, local_micro_alt);
        Ok((input, interval))
    }
}

#[cfg(test)]
mod tests {
    use crate::pitch::{EdoChord, EdoPitch};

    use super::parse_up_down_chord_symbol;

    #[test]
    fn simple_chords_53() {
        assert_eq!(
            parse_up_down_chord_symbol("C", 53).unwrap().to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(0, 53), &[0, 18, 31])
        );
        assert_eq!(
            parse_up_down_chord_symbol("D", 53).unwrap().to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(9, 53), &[0, 18, 31])
        );
        assert_eq!(
            parse_up_down_chord_symbol("^Eb", 53)
                .unwrap()
                .to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(14, 53), &[0, 18, 31])
        );
        assert_eq!(
            parse_up_down_chord_symbol("vEbm", 53)
                .unwrap()
                .to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(12, 53), &[0, 13, 31])
        );
        assert_eq!(
            parse_up_down_chord_symbol("vEb^m", 53)
                .unwrap()
                .to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(12, 53), &[0, 14, 31])
        );
        assert_eq!(
            parse_up_down_chord_symbol("vEb^m7", 53)
                .unwrap()
                .to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(12, 53), &[0, 14, 31, 45])
        );
        assert_eq!(
            parse_up_down_chord_symbol("vEb^m7,11(v7)", 53)
                .unwrap()
                .to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(12, 53), &[0, 14, 31, 43, 22 + 53])
        );
        assert_eq!(
            parse_up_down_chord_symbol("vF#,2no3", 53)
                .unwrap()
                .to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(26, 53), &[0, 9, 31])
        );
        assert_eq!(
            parse_up_down_chord_symbol("vF#,2no3no5", 53)
                .unwrap()
                .to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(26, 53), &[0, 9])
        );
        assert_eq!(
            parse_up_down_chord_symbol("vF#v(v7)no1", 53)
                .unwrap()
                .to_edo_chord(),
            EdoChord::from_relative_degrees(EdoPitch::new(26, 53), &[17, 31, 43])
        );
    }
}
