use std::marker::PhantomData;

use crate::EdoPitch;

pub struct Major;
pub struct MinorNatural;
pub struct MinorHarmonic;
pub struct MinorMelodic;
/// Natural, Harmonic and Melodic minor intervals
pub struct MinorCombined;
/// An iterator that produces a scale, starting from the lowest pitch
pub struct EdoScale<MODE> {
    next_index: usize,
    mode: PhantomData<MODE>,
}
impl<MODE> Iterator for EdoScale<MODE> {
    type Item = EdoPitch;

    fn next(&mut self) -> Option<Self::Item> {
        todo!()
    }
}

pub trait Scale {}
