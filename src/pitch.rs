use crate::note_names::*;
use crate::pitch::parse_chord_symbol::parse_up_down_chord_symbol;
use anyhow::Result;
use std::cmp::Ordering;
use std::convert::TryFrom;
use std::ops::{Add, Sub};
use std::{
    collections::VecDeque,
    sync::{Arc, RwLock},
};
use thiserror::Error;
mod parse_chord_symbol;
pub mod scale;

type Frequency = f32;

type PitchReference = Arc<RwLock<Pitch>>;

#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Pitch {
    Edo(EdoPitch),
    Freq(FreqPitch),
}
impl Pitch {
    pub fn edo(degree: i32, divisions: u32) -> Pitch {
        Pitch::Edo(EdoPitch::new(degree, divisions))
    }
    pub fn freq(freq: f32) -> Pitch {
        Pitch::Freq(FreqPitch::new(freq))
    }
    pub fn transpose(&mut self, interval: Interval) {
        match self {
            Pitch::Edo(ref mut edo_pitch) => {
                if let Interval::Edo(ei) = interval {
                    edo_pitch.transpose_by_interval(ei);
                }
            }
            Pitch::Freq(freq_pitch) => {
                if let Interval::Freq(f) = interval {
                    freq_pitch.transpose(f);
                }
            }
        }
    }
    pub fn to_edo(&self, divisions: u32, root_freq: f32) -> EdoPitch {
        match self {
            Pitch::Edo(edo_pitch) => edo_pitch.to_edo(divisions),
            Pitch::Freq(freq_pitch) => freq_pitch.to_edo_pitch(divisions, root_freq),
        }
    }
    pub fn to_freq_f32(&self, root_freq: f32) -> f32 {
        match self {
            Pitch::Edo(edo_pitch) => edo_pitch.frequency(root_freq),
            Pitch::Freq(freq_pitch) => freq_pitch.frequency(),
        }
    }
}

impl PartialOrd for Pitch {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        if std::mem::discriminant(self) == std::mem::discriminant(other) {
            match (self, other) {
                (&Pitch::Edo(ref p1), &Pitch::Edo(ref p2)) => p1.partial_cmp(p2),
                (Pitch::Freq(ref p1), Pitch::Freq(ref p2)) => p1.partial_cmp(p2),
                _ => None,
            }
        } else {
            None
        }
    }
}

// impl PartialEq for Pitch {
//     fn eq(&self, other: &Self) -> bool {
//         if std::mem::discriminant(self) == std::mem::discriminant(other) {
// 	    match (self, other) {
// 	    }
// 	} else {
// 	    // Don't try to compare pitches of different types right now
// 	    // TODO: If pitches themselves include a reference pitch that leads to a
// 	    // frequency in the future it would be possible to compare pitches of differnt
// 	    // types. In this case, they should be tested to be within a certain margin
// 	    // of error from each other. Maybe it's better to make that a separate
// 	    // function.
// 	    false
// 	}
//     }
// }

#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Copy, Debug)]
pub enum Interval {
    Edo(EdoInterval),
    Freq(FreqPitch),
}

impl Interval {
    pub fn edo(degree: i32, divisions: u32) -> Self {
        Self::Edo(EdoInterval::new(degree, divisions))
    }
}

#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct FreqPitch {
    frequency: Frequency,
}
impl FreqPitch {
    pub fn new(frequency: Frequency) -> Self {
        Self { frequency }
    }
    /// Convert a frequency to an edo degree. The `root_freq` is the
    /// frequency of degree 0 in the system.
    /// ```
    ///
    /// # use musical_matter::pitch::*;
    /// // A FreqPitch converted to an EdoPitch using a root_freq of half the
    /// // original freq_pitch frequency should end up being represented as an octave.
    /// let freq_pitch = FreqPitch::new(440.0);
    /// let edo_pitch = EdoPitch::new(12, 12);
    /// assert_eq!(freq_pitch.to_edo_pitch(12, 220.0), edo_pitch);
    /// ```
    pub fn to_edo_pitch(&self, edo: u32, root_freq: f32) -> EdoPitch {
        EdoPitch::new(
            (edo as f32 * (self.frequency / root_freq).log2()).round() as i32,
            edo,
        )
    }
    pub fn frequency(&self) -> Frequency {
        self.frequency
    }
    pub fn transpose(&mut self, interval: FreqPitch) {
        self.frequency += interval.frequency();
    }
}

/// Pitches and intervals between pitches are very similar in how they are
/// represented and how you add them together, but certain things differ. They
/// are called different things (note names or interval names) and adding two
/// pitches together makes little sense, whereas adding an interval to a pitch
/// is logical.
///
/// # Equality
/// The `degree` is represented by an f32, but NaN, inf or -inf are not valid values and will cause a panic. Therefore, the struct can be Eq.

#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Copy, Debug, Eq, Hash)]
pub struct EdoInterval {
    degree: i32,
    divisions: u32,
}

impl PartialEq for EdoInterval {
    fn eq(&self, other: &Self) -> bool {
        // Check that the respective degrees are the same compared to
        // their octave divisions. This way intervals from different
        // edos can be compared.

        self.degree * other.divisions as i32 == other.degree * self.divisions as i32
    }
}

impl Sub for EdoInterval {
    type Output = Self;

    fn sub(mut self, mut rhs: Self) -> Self::Output {
        if self.divisions == rhs.divisions {
            self.degree -= rhs.degree;
            self
        } else {
            self.degree *= rhs.divisions as i32;
            rhs.degree *= self.divisions as i32;
            self.divisions *= rhs.divisions;
            self.degree -= rhs.degree;
            // Reduce down the divisions of the interval. We do this to reduce the risk of overflow when doing lots of arithmetic on an interval.
            loop {
                if self.divisions % 2 == 0 && self.degree % 2 == 0 {
                    self.degree /= 2;
                    self.divisions /= 2;
                } else if self.divisions % 3 == 0 && self.degree % 3 == 0 {
                    self.degree /= 3;
                    self.divisions /= 3;
                } else if self.divisions % 5 == 0 && self.degree % 5 == 0 {
                    self.degree /= 5;
                    self.divisions /= 5;
                } else if self.divisions % 7 == 0 && self.degree % 7 == 0 {
                    self.degree /= 7;
                    self.divisions /= 7;
                } else {
                    break;
                }
            }
            self
        }
    }
}

impl EdoInterval {
    const INFINITE: Self = Self {
        degree: i32::MAX,
        divisions: 0,
    };
    pub fn new(degree: i32, divisions: u32) -> Self {
        Self { degree, divisions }
    }
    pub fn cmp_edo_independent(&self, other: Self) -> Ordering {
        if self.divisions == other.divisions {
            return self.degree.cmp(&other.degree);
        }
        let p0 = self.degree * other.divisions as i32;
        let p1 = other.degree * self.divisions as i32;
        if p0 == p1 {
            Ordering::Equal
        } else if p0 > p1 {
            Ordering::Greater
        } else {
            Ordering::Less
        }
    }
    pub fn ascending(degree: i32, divisions: u32) -> Self {
        // If a descending degree is passed in, convert it to an ascending interval
        let degree = degree.abs();
        Self { degree, divisions }
    }
    pub fn descending(degree: i32, divisions: u32) -> Self {
        // If an ascending degree (positive) is passed in, convert it to a descending interval.
        let degree = degree.abs() * -1;
        Self { degree, divisions }
    }
    pub fn from_name<T: AsRef<str> + std::fmt::Display>(name: T, divisions: u32) -> Option<Self> {
        if let Some(degree) = interval_name_to_degree(name, divisions) {
            Some(Self { degree, divisions })
        } else {
            None
        }
    }
    pub fn transpose_octaves(&mut self, octaves: i32) {
        self.degree += self.divisions as i32 * octaves;
    }
    pub fn to_linear_ratio(&self) -> f32 {
        self.degree as f32 / self.divisions as f32
    }
    pub fn to_edo_pitch(&self) -> EdoPitch {
        EdoPitch::new(self.degree, self.divisions)
    }
    #[must_use]
    pub fn to_first_octave(mut self) -> Self {
        while self.degree < 0 {
            self.degree += self.divisions as i32;
        }
        self.degree = self.degree % self.divisions as i32;
        self
    }
    /// Checks if the given interval is closer to the given 12tet interval than it is to any other interval.
    /// # Examples:
    /// ```
    /// # use musical_matter::pitch::*;
    /// let major_third_5_limit = EdoInterval::new(17, 53);
    /// let major_third_pythagorean = EdoInterval::new(18, 53);
    /// let minor_third = EdoInterval::new(14, 53);
    /// assert!(major_third_5_limit.matches_12_interval_name("M3").unwrap());
    /// assert!(major_third_pythagorean.matches_12_interval_name("M3").unwrap());
    /// assert!(minor_third.matches_12_interval_name("m3").unwrap());
    /// assert!(!minor_third.matches_12_interval_name("M3").unwrap());
    /// ```
    pub fn matches_12_interval_name<T: AsRef<str>>(&self, name: T) -> Result<bool, PitchError> {
        if let Some(degrees12) = EDO12INTERVALS.get(name.as_ref()) {
            let self_as_12 = self.to_linear_ratio() * 12.0;
            Ok((self_as_12 >= (*degrees12 as f32 - 0.5))
                && (self_as_12 < (*degrees12 as f32 + 0.5)))
        } else {
            Err(PitchError::NameCannotBeFound(name.as_ref().to_owned()))
        }
    }
    pub fn octave(divisions: u32) -> Self {
        Self {
            degree: divisions as i32,
            divisions,
        }
    }
}

fn interval_name_to_degree<T: AsRef<str> + std::fmt::Display>(
    name: T,
    divisions: u32,
) -> Option<i32> {
    match divisions {
        12 => {
            if let Some(d) = EDO12INTERVALS.get(name.as_ref()) {
                return Some(*d);
            }
        }
        _ => (),
    }
    eprintln!(
        "Failed to find interval by name, divisions: {}, interval_name: {}",
        divisions, name
    );
    None
}

/// Represents a pitch in some EDO system. The underlying representation is currently the same as EdoInterval, but they are used differently.
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct EdoPitch {
    pub degree: i32,
    pub divisions: u32,
}

impl EdoPitch {
    pub fn new(degree: i32, divisions: u32) -> Self {
        Self { degree, divisions }
    }
    pub fn eq_edo_independent(&self, other: &EdoPitch) -> bool {
        self.degree * other.divisions as i32 == other.degree * self.divisions as i32
    }
    pub fn cmp_edo_independent(&self, other: Self) -> Ordering {
        let p0 = self.degree * other.divisions as i32;
        let p1 = other.degree * self.divisions as i32;
        if self.eq_edo_independent(&other) {
            Ordering::Equal
        } else if p0 > p1 {
            Ordering::Greater
        } else {
            Ordering::Less
        }
    }

    pub fn to_name(&self) -> String {
        let name = match self.divisions {
            12 => "", // TODO: A smart function for converting a degree to a note name
            _ => "",
        };
        name.to_owned()
    }

    pub fn to_edo(&self, divisions: u32) -> Self {
        if divisions != self.divisions {
            let degree =
                ((self.degree as f32 / self.divisions as f32) * divisions as f32).round() as i32;
            Self { degree, divisions }
        } else {
            self.clone()
        }
    }

    pub fn to_linear_ratio(&self) -> f32 {
        self.degree as f32 / self.divisions as f32
    }

    /// Return the equivalent `FreqPitch` where degree 0 corresponds to the `root_freq` argument.
    #[must_use]
    pub fn to_freq_pitch(&self, root_freq: Frequency) -> FreqPitch {
        FreqPitch::new(self.frequency(root_freq))
    }

    /// Generate the interval between this EdoPitch and another EdoPitch
    #[must_use]
    pub fn interval(&self, other: Self) -> EdoInterval {
        if self.divisions == other.divisions {
            EdoInterval::new(other.degree - self.degree, self.divisions)
        } else {
            EdoInterval::new(
                other.degree * self.divisions as i32 - self.degree * other.divisions as i32,
                self.divisions * other.divisions,
            )
        }
    }

    #[must_use]
    pub fn from_add_degree(&self, degree: i32) -> Self {
        Self::new(self.degree + degree, self.divisions)
    }

    #[must_use]
    pub fn transpose_by_interval(self, interval: EdoInterval) -> Self {
        if interval.divisions == self.divisions {
            Self::new(self.degree + interval.degree, self.divisions)
        } else {
            let interval_in_this_edo =
                (interval.to_linear_ratio() * self.divisions as f32).round() as i32;
            Self::new(self.degree + interval_in_this_edo, self.divisions)
        }
    }
    #[must_use]
    pub fn to_first_octave(mut self) -> Self {
        while self.degree < 0 {
            self.degree += self.divisions as i32;
        }
        self.degree = self.degree % self.divisions as i32;
        self
    }

    #[must_use]
    pub fn transpose_degree(mut self, degree: i32) -> Self {
        self.degree += degree;
        self
    }
    pub fn frequency(&self, root_freq: f32) -> f32 {
        root_freq * 2_f32.powf(self.degree as f32 / self.divisions as f32)
    }
    /// The octave is the number of times th divisions go into the degree, including negative number of times. The octave is always rounded down.
    /// ```
    /// # use musical_matter::pitch::*;
    /// let ep = EdoPitch::new(-13, 12);
    /// assert_eq!(ep.octave(), -2);
    /// ```
    pub fn octave(&self) -> i32 {
        (self.degree as f32 / self.divisions as f32).floor() as i32
    }
    #[must_use]
    pub fn as_interval(self) -> EdoInterval {
        EdoInterval::new(self.degree, self.divisions)
    }
}

impl PartialOrd for EdoPitch {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let r1 = self.degree * other.divisions as i32;
        let r2 = other.degree * self.divisions as i32;
        // Comparison with a margin or error for floating point errors
        if r1 == r2 {
            Some(std::cmp::Ordering::Equal)
        } else if r1 > r2 {
            Some(std::cmp::Ordering::Greater)
        } else if r1 < r2 {
            Some(std::cmp::Ordering::Less)
        } else {
            None
        }
    }
}

impl Add for EdoPitch {
    type Output = EdoPitch;

    fn add(self, rhs: Self) -> Self::Output {
        if self.divisions == rhs.divisions {
            self.from_add_degree(rhs.degree)
        } else {
            self.transpose_by_interval(rhs.as_interval())
        }
    }
}
impl Sub for EdoPitch {
    type Output = EdoPitch;

    fn sub(self, mut rhs: Self) -> Self::Output {
        rhs.degree *= -1;
        self + rhs
    }
}

/// A representation of a chord. The root is only included in the
/// resulting pitches if a P1 is included as an interval.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
pub struct EdoChord {
    pub root: EdoPitch,
    /// Intervals relative to the root
    intervals: Vec<EdoInterval>,
}

// impl PartialEq for EdoChord {
//     fn eq(&self, other: &Self) -> bool {
//         // Two chords are equal if they have the same root and contain the same intervals
//         if self.root != other.root {
//             return false;
//         }
//         if self.intervals.len() != other.intervals.len() {
//             return false;
//         }
//         for interval in &self.intervals {
//             if !other.intervals.contains(interval) {
//                 return false;
//             }
//         }
//         true
//     }
// }

impl EdoChord {
    pub fn new(root: EdoPitch, mut intervals: Vec<EdoInterval>) -> Self {
        // Make sure the list of intervals is sorted
        intervals.sort_by(|a: &EdoInterval, b| {
            a.to_linear_ratio()
                .partial_cmp(&b.to_linear_ratio())
                .unwrap()
        });
        Self { root, intervals }
    }
    pub fn push_interval(&mut self, interval: EdoInterval) {
        self.intervals.push(interval);
    }
    pub fn from_relative_degrees(root: EdoPitch, degrees: &[i32]) -> Self {
        let mut intervals: Vec<_> = degrees
            .iter()
            .map(|degree| EdoInterval::new(*degree, root.divisions))
            .collect();
        // Make sure the list of intervals is sorted
        intervals.sort_by(|a: &EdoInterval, b| {
            a.to_linear_ratio()
                .partial_cmp(&b.to_linear_ratio())
                .unwrap()
        });
        Self { root, intervals }
    }
    /// Transpose the whole chord by an interval
    pub fn transpose_interval(&mut self, interval: EdoInterval) -> &mut Self {
        self.root.transpose_by_interval(interval);
        self
    }
    /// Set the intervals based on degrees, each relative to the current root.
    pub fn set_intervals_from_relative_degrees(&mut self, degrees: &[i32]) -> &mut Self {
        self.intervals = degrees
            .iter()
            .map(|degree| EdoInterval::new(*degree, self.root.divisions))
            .collect();
        // Make sure the list of intervals is sorted
        self.intervals.sort_by(|a: &EdoInterval, b| {
            a.to_linear_ratio()
                .partial_cmp(&b.to_linear_ratio())
                .unwrap()
        });
        self
    }
    pub fn set_intervals_from_absolute_degrees(&mut self, degrees: &[i32]) -> &mut Self {
        self.intervals = degrees
            .iter()
            .map(|degree| EdoInterval::new(*degree - self.root.degree, self.root.divisions))
            .collect();

        // Make sure the list of intervals is sorted
        self.intervals.sort_by(|a: &EdoInterval, b| {
            a.to_linear_ratio()
                .partial_cmp(&b.to_linear_ratio())
                .unwrap()
        });
        self
    }
    pub fn get_bass(&self) -> Option<EdoPitch> {
        if self.intervals.len() == 0 {
            return None;
        }
        // Find lowest interval
        let mut lowest_octave_ratio = f32::MAX;
        let mut lowest_interval_index = 0;
        for (i, interval) in self.intervals.iter().enumerate() {
            if interval.to_linear_ratio() < lowest_octave_ratio {
                lowest_octave_ratio = interval.to_linear_ratio();
                lowest_interval_index = i;
            }
        }

        // Return a new pitch which is the root transposed by that interval
        Some(
            self.root
                .transpose_by_interval(self.intervals[lowest_interval_index]),
        )
    }
    pub fn num_pitches(&self) -> usize {
        // A perfect prime as an interval is needed to have the root note in the chord
        self.intervals.len()
    }

    pub fn to_pitch_sequence(&self) -> PitchSequence {
        let pitches = self
            .intervals
            .iter()
            .map(|interval| Some(Pitch::Edo(self.root.transpose_by_interval(*interval))))
            .collect();
        PitchSequence::new(pitches)
    }
    pub fn to_pitches(&self) -> Vec<Pitch> {
        self.intervals
            .iter()
            .map(|interval| Pitch::Edo(self.root.transpose_by_interval(*interval)))
            .collect::<Vec<Pitch>>()
    }
    pub fn to_edo_pitches(&self) -> Vec<EdoPitch> {
        self.intervals
            .iter()
            .map(|&interval| self.root.transpose_by_interval(interval))
            .collect::<Vec<_>>()
    }
    pub fn arpeggiate(&self, octaves: i32, start_octave: i32) -> PitchSequence {
        let mut pitches = Vec::with_capacity(self.intervals.len() * octaves as usize);
        for oct in 0..octaves {
            for interval in &self.intervals {
                let mut pitch = self.root;
                let mut interval = *interval;
                interval.transpose_octaves(oct + start_octave);
                pitch.transpose_by_interval(interval);
                pitches.push(Some(Pitch::Edo(pitch)));
            }
        }
        PitchSequence::new(pitches)
    }

    /// Returns a list of absolute degrees. The transposition depends on the root. The root is included only if a P1 exists in the chord.
    pub fn to_degrees(&self) -> Vec<i32> {
        self.intervals
            .iter()
            .map(|interval| self.root.transpose_by_interval(*interval).degree)
            .collect::<Vec<i32>>()
    }
    /// Returns a list of relaive degrees compared to the root. The root (0) is included only if a P1 exists in the chord.
    pub fn to_relative_degrees(&self) -> Vec<i32> {
        self.intervals
            .iter()
            .map(|interval| interval.degree)
            .collect::<Vec<i32>>()
    }

    pub fn is_major(&self) -> bool {
        for interval in &self.intervals {
            if let Ok(true) = interval.matches_12_interval_name("M3") {
                return true;
            }
        }
        return false;
    }
    pub fn is_minor(&self) -> bool {
        for interval in &self.intervals {
            if let Ok(true) = interval.matches_12_interval_name("m3") {
                return true;
            }
        }
        return false;
    }
    pub fn get_degree(&self, note: ChordNoteSelector) -> Option<EdoPitch> {
        match note {
            ChordNoteSelector::Bass => self.get_bass(),
            ChordNoteSelector::Root => Some(self.root),
            ChordNoteSelector::NFromTop(n) => self
                .intervals
                .iter()
                .rev()
                .nth(n)
                .map(|interval| self.root.clone().transpose_by_interval(*interval)),
            ChordNoteSelector::NFromBottom(n) => self
                .intervals
                .iter()
                .nth(n)
                .map(|interval| self.root.clone().transpose_by_interval(*interval)),
            ChordNoteSelector::NearestPitch(pitch) => {
                // Turn into pitches and get the nearest one
                let mut smallest_distance = EdoInterval::INFINITE;
                let mut nearest_pitch = None;
                let pitches = self.to_edo_pitches();
                for p in pitches {
                    let interval = pitch.interval(p);
                    if interval.cmp_edo_independent(smallest_distance) == Ordering::Less {
                        nearest_pitch = Some(p);
                        smallest_distance = interval;
                    }
                }
                nearest_pitch
            }
            ChordNoteSelector::NearestInterval(interval) => {
                let mut smallest_distance = f32::MAX;
                let mut nearest_interval = None;
                for i in &self.intervals {
                    let distance = (interval - *i).to_linear_ratio().abs();
                    if distance < smallest_distance {
                        nearest_interval = Some(*i);
                        smallest_distance = distance;
                    }
                }
                nearest_interval.map(|i| self.root.transpose_by_interval(i))
            }
        }
    }
}

/// Sequence of pitches and rests (represented by None) to be used for
/// pitch material and combined with rhythmical material in a
/// MusicStream
#[derive(Clone, Debug)]
pub struct PitchSequence {
    pitches: VecDeque<Option<Pitch>>,
}

impl PitchSequence {
    pub fn new(pitches: Vec<Option<Pitch>>) -> Self {
        Self {
            pitches: pitches.into(),
        }
    }
    pub fn from_edo_degrees(degrees: Vec<i32>, edo: u32) -> Self {
        let pitches = degrees
            .into_iter()
            .map(|degree| Some(Pitch::edo(degree, edo)))
            .collect();
        Self { pitches }
    }
    /// Return a new PitchSequence with the pitches reordered
    /// according to the indices in the input Vec
    pub fn in_order(&self, pitch_order: Vec<usize>) -> Self {
        let mut new_pitches = Vec::with_capacity(pitch_order.len());
        // TODO: When copying the pitches we may need to assign new IDs if each pitch will have an ID in the future.
        for index in pitch_order {
            if index < self.pitches.len() {
                new_pitches.push(self.pitches[index].clone())
            } else {
                eprintln!(
                    "PitchSequence::in_order: index received was out of bounds: {}, max: {}",
                    index,
                    self.pitches.len()
                );
            }
        }
        Self::new(new_pitches)
    }
    pub fn to_degrees(&self) -> Vec<Option<i32>> {
        let degrees = self
            .pitches
            .iter()
            .map(|pitch| {
                if let Some(pitch) = pitch {
                    if let Pitch::Edo(edo_pitch) = pitch {
                        Some(edo_pitch.degree)
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
            .collect();
        degrees
    }
    pub fn push(&mut self, pitch: Option<Pitch>) -> &mut Self {
        self.pitches.push_back(pitch);
        self
    }
    pub fn insert(&mut self, index: usize, pitch: Option<Pitch>) -> &mut Self {
        self.pitches.insert(index, pitch);
        self
    }
    pub fn remove(&mut self, i: usize) {
        self.pitches.remove(i);
    }
    pub fn transpose(&mut self, interval: Interval) -> &mut Self {
        for pitch in &mut self.pitches {
            if let Some(ref mut p) = pitch {
                p.transpose(interval)
            }
        }
        self
    }
    pub fn get_pitches(&mut self) -> &mut [Option<Pitch>] {
        self.pitches.make_contiguous()
    }
    pub fn get(&self, i: usize) -> Option<Pitch> {
        self.pitches[i]
    }
    pub fn len(&self) -> usize {
        self.pitches.len()
    }
    pub fn last(&self) -> Option<Pitch> {
        if self.pitches.len() > 0 {
            self.pitches[self.pitches.len() - 1]
        } else {
            None
        }
    }
    pub fn extend_sequence(&mut self, pitch_sequence: PitchSequence) -> &mut Self {
        self.pitches.extend(pitch_sequence.pitches.into_iter());
        self
    }
    pub fn rotate(&mut self, num: i32) -> &mut Self {
        if num > 0 {
            self.pitches.rotate_right(num as usize);
        } else {
            self.pitches.rotate_left((-num) as usize);
        }
        self
    }
    pub fn make_ascending(&mut self) -> &mut Self {
        let mut last_pitch = self.pitches[0];
        let octave_interval = Interval::edo(2, 2); // What edo doesn't matter since it is reduced to a ratio
        for mut opt_pitch in &mut self.pitches {
            if let Some(pitch) = &mut opt_pitch {
                if let Some(last_pitch) = &last_pitch {
                    while *pitch < *last_pitch {
                        pitch.transpose(octave_interval);
                    }
                }
                last_pitch = Some(pitch.clone());
            }
        }
        self
    }
    pub fn make_descending(&mut self) -> &mut Self {
        let mut last_pitch = self.pitches[0];
        let octave_interval = Interval::edo(-12, 12); // What edo doesn't matter since it is reduced to a ratio
        for mut opt_pitch in &mut self.pitches {
            if let Some(pitch) = &mut opt_pitch {
                if let Some(last_pitch) = &last_pitch {
                    while *pitch > *last_pitch {
                        pitch.transpose(octave_interval);
                    }
                }
                last_pitch = Some(pitch.clone());
            }
        }
        self
    }
    pub fn repeat(mut self, repeats: u32) -> Self {
        let num_elements = repeats * self.pitches.len() as u32;
        self.pitches = self
            .pitches
            .into_iter()
            .cycle()
            .take(num_elements as usize)
            .collect();
        self
    }
}

/// A semantic representation of a chord that can be turned into a chord symbol
/// in ups and downs notation. In almost all EDOs, the thirds are imperfect
/// intervals, the fourths are perfect intervals etc. A few EDOs lack major/minor intervals.
#[derive(Clone, Debug)]
pub struct EdoChordSemantic {
    root: EdoPitch,
    exclude_root: bool, // if the root is excluded, the chord is still written the same except for the no1
    bass: Option<EdoIntervalSemantic>,
    second: Option<EdoIntervalSemantic>,
    third: Option<EdoIntervalSemantic>,
    fourth: Option<EdoIntervalSemantic>,
    fifth: Option<EdoIntervalSemantic>,
    sixth: Option<EdoIntervalSemantic>,
    seventh: Option<EdoIntervalSemantic>,
    ninth: Option<EdoIntervalSemantic>,
    eleventh: Option<EdoIntervalSemantic>,
    thirteenth: Option<EdoIntervalSemantic>,
}

impl EdoChordSemantic {
    pub fn from_root(pitch: EdoPitch) -> Self {
        Self {
            root: pitch,
            exclude_root: false,
            bass: None,
            second: None,
            third: None,
            fourth: None,
            fifth: None,
            sixth: None,
            seventh: None,
            ninth: None,
            eleventh: None,
            thirteenth: None,
        }
    }
    /// Create a chord from a chord symbol. E.g.
    /// - C
    /// - Dm
    /// - ^F#v3
    pub fn from_chord_symbol(symbol: &str, edo: u32) -> Option<Self> {
        if let Ok(chord) = parse_up_down_chord_symbol(symbol, edo) {
            Some(chord)
        } else {
            None
        }
    }
    pub fn from_root_major(root: EdoPitch) -> Self {
        let third = match EdoCategory::from_edo(root.divisions) {
            EdoCategory::Superflat => todo!(),
            EdoCategory::Perfect => {
                // No impoerfect intervals
                EdoIntervalSemantic {
                    quality: IntervalQuality::Perfect(PerfectQuality::Perfect),
                    deviation: 0,
                    edo: root.divisions,
                    number: 3,
                }
            }
            EdoCategory::Diatonic => {
                // We have a major third
                EdoIntervalSemantic {
                    quality: IntervalQuality::Imperfect(ImperfectQuality::Major),
                    deviation: 0,
                    edo: root.divisions,
                    number: 3,
                }
            }
            EdoCategory::Pentatonic => todo!(),
            EdoCategory::Supersharp => todo!(),
            EdoCategory::Trivial => todo!(),
        };
        // TODO: Some edo categories don't have perfect intervals so it would have to be a major fifth here
        let fifth = match EdoCategory::from_edo(root.divisions) {
            EdoCategory::Superflat => todo!(),
            EdoCategory::Diatonic | EdoCategory::Perfect => EdoIntervalSemantic {
                quality: IntervalQuality::Perfect(PerfectQuality::Perfect),
                deviation: 0,
                edo: root.divisions,
                number: 5,
            },
            EdoCategory::Pentatonic => todo!(),
            EdoCategory::Supersharp => todo!(),
            EdoCategory::Trivial => todo!(),
        };
        Self {
            root,
            exclude_root: false,
            bass: None,
            second: None,
            third: Some(third),
            fourth: None,
            fifth: Some(fifth),
            sixth: None,
            seventh: None,
            ninth: None,
            eleventh: None,
            thirteenth: None,
        }
    }
    pub fn set_root(&mut self, root: EdoPitch) {
        self.root = root;
    }
    pub fn transpose(&mut self, interval: EdoInterval) {
        self.root = self.root.transpose_by_interval(interval);
        self.root = self.root.to_first_octave();
    }
    pub fn set_interval(&mut self, interval: EdoIntervalSemantic) {
        let number = interval.number;
        match number {
            1 => self.exclude_root = false,
            2 => self.second = Some(interval),
            3 => self.third = Some(interval),
            4 => self.fourth = Some(interval),
            5 => self.fifth = Some(interval),
            6 => self.sixth = Some(interval),
            7 => self.seventh = Some(interval),
            9 => self.ninth = Some(interval),
            11 => self.eleventh = Some(interval),
            13 => self.thirteenth = Some(interval),
            _ => eprintln!(
                "Tried to add an interval with an unsupported number to a chord: {interval:?}"
            ),
        }
    }
    pub fn remove_interval(&mut self, number: u32) {
        match number {
            1 => self.exclude_root = true,
            2 => self.second = None,
            3 => self.third = None,
            4 => self.fourth = None,
            5 => self.fifth = None,
            6 => self.sixth = None,
            7 => self.seventh = None,
            9 => self.ninth = None,
            11 => self.eleventh = None,
            13 => self.thirteenth = None,
            _ => eprintln!("Tried to remove an interval with an unsupported number: {number}"),
        }
    }
    /// Generates a chord symbol in ups and downs notation from the chord
    pub fn to_chord_symbol(&self) -> String {
        // In ups and downs notation, a few things need to be kept in mind:
        // 1. There are two types of alterations; I will call them macro alterations (m, M, a, d, ~) and micro alterations (^, v).
        // 1. The first alteration after the pitch applies to the sixth, third, seventh and eleventh. A different seventh needs to be specifically notated using e.g. C^m13(v7)
        // 2. I prefer a(ugmented) and d(iminished) to # and b because they are less confusing. Otherwise ^C#(^#5)
        // 3. Alterations have to come inside parentheses.
        // 4. Added pitches are written after a comma, read as "add", and never in parentheses
        // 5. Context is formed by the default context (M2, M3, P4, P5, M6, m7, M9, P11, M13 as usual) and the macro
        //    alteration right after the root. The macro alteration after the root behaves differently from macro alterations applied to a single pitch:
        // 5a. 'm' e.g. Cm13 turns 3 into minor
        // 5b. 'M' e.g. CM7,v13 turns 7 into major
        // 5c. 'd' e.g. Cd13 turns 3 into minor, 5 into diminished and 7 into diminished
        // 5d. 'a' e.g. Ca9 turns 5 into augmented
        // 6. Alterations to the third e.g. C7(^3) use the same macro alteration (major/minor/mid) as the main chord. C7(^3) has a ^M3, Cm7(^3) has a ^m3, C~7(^3) has a ^~3. Same goes for other pitches.
        // 7. Adding a micro alteration resets any existing micro alteration applied earlier e.g. Cv(^7) = { C vE G ^Bb }.
        // 8. Adding a macro alteration to a 7 is relative to the M7 e.g. (bb7) or (d7) relative to C is Bbb.
        // 9. If several pitches are altered or added only one set of parentheses/one comma are/is needed if the interval degrees are separated by an alteration e.g. CM9(^3^5). An example of when this is not possible is Cv,6,9
        //
        // Examples (standard notation | ups and downs notation)
        // Cm7b5 | Cd,7 OR Cm7(b5)
        //
        // Cm(M7)

        match EdoCategory::from_edo(self.root.divisions) {
            EdoCategory::Superflat => todo!(),
            EdoCategory::Perfect => todo!(),
            EdoCategory::Diatonic => {
                // Pitches that require an additional alteration in parentheses, e.g. Cv9(vv7)
                // format: (micro, macro, number)
                let mut altered_pitches = vec![];

                // Do we have a fifth and a third?
                // TODO: In the ups and downs spec '2' and '4' are used instead of sus2 and sus4. I don't know if I agree with this.
                let suspension = if self.third.is_some() {
                    // normal
                    ""
                } else if self.second.is_some() && self.fourth.is_none() {
                    "sus2"
                } else if self.second.is_none() && self.fourth.is_some() {
                    "sus4"
                } else if self.second.is_some() && self.fourth.is_some() {
                    "sus24"
                } else {
                    ""
                };
                let mut main_macro_alt = if self.third.is_some() && self.fifth.is_some() {
                    match self.fifth.unwrap().quality {
                        IntervalQuality::Perfect(PerfectQuality::Perfect) => {
                            match self.third.unwrap().quality {
                                IntervalQuality::Imperfect(ImperfectQuality::Major) => "",
                                IntervalQuality::Imperfect(ImperfectQuality::Minor) => "m",
                                IntervalQuality::Imperfect(ImperfectQuality::Mid) => "~",
                                _ => {
                                    // diminished or augmented third, unsupported
                                    panic!("Unsupported third in diatonic EDO")
                                }
                            }
                        }
                        IntervalQuality::Perfect(PerfectQuality::Augmented) => {
                            match self.third.unwrap().quality {
                                IntervalQuality::Imperfect(ImperfectQuality::Major) => "a",
                                IntervalQuality::Imperfect(ImperfectQuality::Minor) => "m",
                                IntervalQuality::Imperfect(ImperfectQuality::Mid) => "~",
                                _ => {
                                    // diminished or augmented third, unsupported
                                    panic!("Unsupported third in diatonic EDO")
                                }
                            }
                        }
                        IntervalQuality::Perfect(PerfectQuality::Diminished) => {
                            match self.third.unwrap().quality {
                                IntervalQuality::Imperfect(ImperfectQuality::Major) => "",
                                IntervalQuality::Imperfect(ImperfectQuality::Minor) => "d",
                                IntervalQuality::Imperfect(ImperfectQuality::Mid) => "~",
                                _ => {
                                    // diminished or augmented third, unsupported
                                    panic!("Unsupported third in diatonic EDO")
                                }
                            }
                        }
                        _ => panic!("Unsupported fifth in diatonic EDO"),
                    }
                } else if let Some(third) = self.third {
                    // There is no fifth, but a third
                    match third.quality {
                        IntervalQuality::Imperfect(ImperfectQuality::Major) => "",
                        IntervalQuality::Imperfect(ImperfectQuality::Minor) => "m",
                        IntervalQuality::Imperfect(ImperfectQuality::Mid) => "~",
                        _ => {
                            // diminished or augmented third, unsupported
                            panic!("Unsupported third in diatonic EDO")
                        }
                    }
                } else if let Some(fifth) = self.fifth {
                    // There is no third, but there is a fifth
                    match fifth.quality {
                        IntervalQuality::Perfect(perfect_quality) => match perfect_quality {
                            PerfectQuality::Augmented => "a",
                            PerfectQuality::Perfect => "",
                            PerfectQuality::Diminished => "",
                            PerfectQuality::Mid => "",
                        },
                        _ => panic!("Unsupported fifth in diatonic EDO"),
                    }
                } else {
                    ""
                };
                // The main_micro_alt modifies the 3rd, 6th, 7th and 11th
                // Uses the labeled loop hack to break out early, in future versions of Rust a labeled block will be allowed instead
                #[allow(clippy::never_loop)]
                let main_micro_alt: i32 = 'block: loop {
                    {
                        // If all the affected pitches use the same micro_alt (ups and downs), use that. If they use different micro_alts, use the most common one. If it's a tie, use that of the lowest number interval.
                        let mut alts = vec![];
                        let intervals = [self.third, self.sixth, self.seventh, self.eleventh];
                        for interval in &intervals {
                            if let Some(interval) = interval {
                                alts.push(interval.deviation);
                            }
                        }
                        if alts.is_empty() {
                            // None of these intervals exist in the chord, abort;
                            break 'block 0;
                        }
                        let identical = {
                            let mut k = alts.clone();
                            k.sort();
                            k.dedup();
                            k.len() == 1
                        };
                        if identical {
                            // They are all the same, and we checked for emptyness earlier
                            break 'block alts[0];
                        }
                        let most_common_alt = {
                            let mut highest_count = 0;
                            let mut most_common_alt = 0;
                            for &alt in &alts {
                                let count = alts.iter().filter(|&&e| e == alt).count();
                                if count > highest_count {
                                    highest_count = count;
                                    most_common_alt = alt;
                                }
                            }
                            most_common_alt
                        };
                        // Before we return, add alterations for the pitches that don't match
                        for interval in &intervals {
                            if let Some(interval) = interval {
                                if interval.deviation != most_common_alt {
                                    altered_pitches.push(interval.chord_alteration_symbols())
                                }
                            }
                        }
                        break 'block most_common_alt;
                    }
                };

                // The highest_contiguous_number is the highest numbered
                // interval that exists in the chord that is not altered
                // compared to the main alteration
                let highest_contiguous_number: i32 = {
                    if self.third.is_none() {
                        if self.exclude_root {
                            0;
                        } else {
                            1;
                        }
                    } else if let Some(interval) = self.third {
                        if !interval.matches_main_alteration(main_macro_alt, main_micro_alt) {
                            if self.exclude_root {
                                0;
                            } else {
                                1;
                            }
                        }
                    } else if self.fifth.is_none() {
                        3;
                    } else if let Some(fifth) = self.fifth {
                        if !fifth.matches_main_alteration(main_macro_alt, main_micro_alt) {
                            3;
                        }
                    } else if self.seventh.is_none() {
                        // the sixth doesn't break the contiguity
                        if self.sixth.is_some() {
                            6;
                        } else {
                            5;
                        }
                    } else if let Some(interval) = self.seventh {
                        if !interval.matches_main_alteration(main_macro_alt, main_micro_alt) {
                            if self.sixth.is_some() {
                                6;
                            } else {
                                5;
                            }
                        }
                    } else if self.ninth.is_none() {
                        7;
                    } else if let Some(interval) = self.ninth {
                        if !interval.matches_main_alteration(main_macro_alt, main_micro_alt) {
                            7;
                        }
                    } else if self.eleventh.is_none() {
                        9;
                    } else if let Some(interval) = self.eleventh {
                        if !interval.matches_main_alteration(main_macro_alt, main_micro_alt) {
                            9;
                        }
                    } else if self.thirteenth.is_none() {
                        11;
                    } else if let Some(interval) = self.thirteenth {
                        if !interval.matches_main_alteration(main_macro_alt, main_micro_alt) {
                            11;
                        }
                    }
                    // all alterations are present
                    13
                };

                // If the 7th exists and is Major, either the main_macro_alt should be M or the 7th needs a separate alteration in parentheses
                if let Some(seventh) = self.seventh {
                    match seventh.quality {
                        IntervalQuality::Imperfect(imperfect_quality) => {
                            match imperfect_quality {
                                ImperfectQuality::Augmented => todo!(),
                                ImperfectQuality::Major => {
                                    // augmented chords have a minor seventh by default
                                    main_macro_alt = match main_macro_alt {
                                        "" => "M",
                                        _ => {
                                            altered_pitches.push(
                                                self.seventh.unwrap().chord_alteration_symbols(),
                                            );
                                            main_macro_alt
                                        }
                                    }
                                }
                                ImperfectQuality::Mid => todo!(),
                                ImperfectQuality::Minor => todo!(),
                                ImperfectQuality::Diminished => todo!(),
                            }
                        }
                        _ => panic!("Unsupported seventh in diatonic EDO"),
                    }
                }

                let mut chord_string = String::new();

                chord_string.push_str(&self.root.to_name());
                if main_micro_alt != 0 {
                    let token = if main_micro_alt > 0 { '^' } else { 'v' };
                    for _ in 0..main_micro_alt.abs() {
                        chord_string.push(token);
                    }
                }
                chord_string.push_str(main_macro_alt);
                if highest_contiguous_number <= 1 {
                    if suspension != "" {
                        chord_string.push_str(suspension);
                    } else if self.fifth.is_some() {
                        chord_string.push('5');
                    }
                } else {
                    chord_string.push_str(&highest_contiguous_number.to_string());
                }

                // add all alterations

                let all_intervals = [
                    self.second,
                    self.third,
                    self.fourth,
                    self.fifth,
                    self.sixth,
                    self.seventh,
                    self.ninth,
                    self.eleventh,
                    self.thirteenth,
                ];
                for interval in all_intervals {
                    if let Some(interval) = interval {
                        if !interval.matches_main_alteration(main_macro_alt, main_micro_alt) {
                            let alt = interval.chord_alteration_symbols();
                            chord_string.push('(');
                            chord_string.push_str(&alt.0);
                            chord_string.push_str(&alt.1);
                            chord_string.push_str(&format!("{}", alt.2));
                            chord_string.push(')');
                        }
                    }
                }

                // TODO: which chord should have an "add"-comma ',' before them? In the examples it seems to be added when its omission would cause confusion as to what pitch is altered.
                // TODO: The order of alterations seems somewhat irregular in the examples. Sometimes
                // add no5 or no3 at the end

                // If no fifth, write no5
                // If no third, write 5 if there are no other pitches e.g. C5 with only C and G, or no3 otherwise
            }
            EdoCategory::Pentatonic => todo!(),
            EdoCategory::Supersharp => todo!(),
            EdoCategory::Trivial => todo!(),
        }

        // let third_deviation_text = self.third.get_deviation_text();
        todo!()
    }
    pub fn apply_main_alterations(&mut self, macro_alt: MacroAlteration, micro_alt: i32) {
        let intervals = [
            &mut self.bass,
            &mut self.second,
            &mut self.third,
            &mut self.fourth,
            &mut self.fifth,
            &mut self.sixth,
            &mut self.seventh,
            &mut self.ninth,
            &mut self.eleventh,
            &mut self.thirteenth,
        ];
        for i in intervals {
            if let Some(i) = i {
                i.apply_main_alteration(macro_alt, micro_alt);
            }
        }
    }
    pub fn intervals(&self) -> Vec<EdoIntervalSemantic> {
        let mut intervals = Vec::new();
        if !self.exclude_root {
            intervals.push(EdoIntervalSemantic::prime(self.root.divisions));
        }
        if let Some(i) = self.second {
            intervals.push(i);
        }
        if let Some(i) = self.third {
            intervals.push(i);
        }
        if let Some(i) = self.fourth {
            intervals.push(i);
        }
        if let Some(i) = self.fifth {
            intervals.push(i);
        }
        if let Some(i) = self.sixth {
            intervals.push(i);
        }
        if let Some(i) = self.seventh {
            intervals.push(i);
        }
        if let Some(i) = self.ninth {
            intervals.push(i);
        }
        if let Some(i) = self.eleventh {
            intervals.push(i);
        }
        if let Some(i) = self.thirteenth {
            intervals.push(i);
        }
        // Only add the bass interval if it is not among the intervals added already
        if let Some(i) = self.bass {
            if !intervals.iter().any(|i2| i == *i2) {
                intervals.push(i);
            }
        }
        intervals
    }
    pub fn to_edo_chord(&self) -> EdoChord {
        let intervals = self
            .intervals()
            .into_iter()
            .map(|i| i.to_edo_interval())
            .collect();
        let mut chord = EdoChord::new(self.root, intervals);
        if let Some(i) = self.bass {
            let mut i = i.to_edo_interval();
            // Add the bass note interval an octave below to make sure it is the lowest, since this is how the bass is determined in an EdoChord
            i.transpose_octaves(-1);
            chord.push_interval(i);
        }
        chord
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum IntervalQuality {
    Perfect(PerfectQuality),
    Imperfect(ImperfectQuality),
}

impl IntervalQuality {
    pub fn to_alteration(&self) -> &'static str {
        match self {
            IntervalQuality::Perfect(q) => match q {
                PerfectQuality::Augmented => "a",
                PerfectQuality::Perfect => "",
                PerfectQuality::Diminished => "d",
                PerfectQuality::Mid => "~",
            },
            IntervalQuality::Imperfect(q) => match q {
                ImperfectQuality::Augmented => "a",
                ImperfectQuality::Major => "M",
                ImperfectQuality::Mid => "~",
                ImperfectQuality::Minor => "m",
                ImperfectQuality::Diminished => "d",
            },
        }
    }
    pub fn apply_macro_alteration(&mut self, macro_alt: MacroAlteration) {
        match self {
            IntervalQuality::Perfect(ref mut perfect_q) => {
                perfect_q.apply_macro_alteration(macro_alt)
            }
            IntervalQuality::Imperfect(ref mut imperfect_q) => {
                imperfect_q.apply_macro_alteration(macro_alt);
            }
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum MacroAlteration {
    None,
    Diminished,
    Augmented,
    Minor,
    Mid,
    Major,
}
impl TryFrom<Option<char>> for MacroAlteration {
    type Error = ();

    fn try_from(value: Option<char>) -> std::result::Result<Self, Self::Error> {
        match value {
            Some('m') => Ok(Self::Minor),
            Some('~') => Ok(Self::Minor),
            Some('M') => Ok(Self::Major),
            Some('a') => Ok(Self::Augmented),
            Some('d') => Ok(Self::Diminished),
            None => Ok(Self::None),
            _ => Err(()),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct EdoIntervalSemantic {
    pub quality: IntervalQuality,
    pub deviation: i32, // essentially the ups or downs
    pub edo: u32,
    pub number: u32, // an interval is not up or down, that depends on the perspective of the starting note, the interval is always a positive distance (except for D1 etc.)
}
impl EdoIntervalSemantic {
    pub fn prime(edo: u32) -> Self {
        Self {
            quality: IntervalQuality::Perfect(PerfectQuality::Perfect),
            deviation: 0,
            edo,
            number: 1,
        }
    }
    /// Creates an interval with the default quality for no alteration in the given EDO
    pub fn from_number(number: u32, edo: u32) -> Result<Self, ()> {
        let mut compare_number = number;
        // bring the number into an octave we have data for
        while compare_number > 13 {
            compare_number -= 8;
        }
        let quality = match number {
            2 => IntervalQuality::Imperfect(ImperfectQuality::Major),
            3 => IntervalQuality::Imperfect(ImperfectQuality::Major),
            10 => IntervalQuality::Imperfect(ImperfectQuality::Major),
            6 => IntervalQuality::Imperfect(ImperfectQuality::Major),
            7 => IntervalQuality::Imperfect(ImperfectQuality::Minor),
            9 => IntervalQuality::Imperfect(ImperfectQuality::Major),
            13 => IntervalQuality::Imperfect(ImperfectQuality::Major),
            5 => IntervalQuality::Perfect(PerfectQuality::Perfect),
            4 => IntervalQuality::Perfect(PerfectQuality::Perfect),
            11 => IntervalQuality::Perfect(PerfectQuality::Perfect),
            1 => IntervalQuality::Perfect(PerfectQuality::Perfect),
            8 => IntervalQuality::Perfect(PerfectQuality::Perfect),
            10 => IntervalQuality::Perfect(PerfectQuality::Perfect),
            _ => return Err(()),
        };
        Ok(Self {
            quality,
            deviation: 0,
            edo,
            number,
        })
    }
    pub fn chord_alteration_symbols(&self) -> (String, String, u32) {
        let micro_alt = if self.deviation == 0 {
            String::new()
        } else if self.deviation > 0 {
            "^".repeat(self.deviation as usize)
        } else {
            "v".repeat(self.deviation.abs() as usize)
        };
        let macro_alt = self.quality.to_alteration();
        (micro_alt.to_owned(), macro_alt.to_owned(), self.number)
    }
    pub fn apply_individual_alteration(
        &mut self,
        macro_alteration: MacroAlteration,
        micro_alteration: i32,
    ) {
        self.deviation = micro_alteration;
        self.quality.apply_macro_alteration(macro_alteration);
    }
    /// Apply the alteration that applies to a whole chord. This is different from the alteration as applied to a specific interval in a chord.
    pub fn apply_main_alteration(
        &mut self,
        macro_alteration: MacroAlteration,
        micro_alteration: i32,
    ) -> Result<(), ()> {
        // Some intervals inherit the main micro alterations while others don't
        match self.number {
            3 | 7 | 6 | 13 => {
                self.deviation = micro_alteration;
            }
            _ => (),
        }
        match self.number {
            4 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),

            13 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
            2 | 9 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
            _ => {
                // TODO: Thesse only work for diatonic EDOs
                match macro_alteration {
                    MacroAlteration::None => match self.number {
                        3 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        5 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),
                        6 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        7 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Minor),
                        11 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),
                        _ => return Err(()),
                    },
                    MacroAlteration::Minor => match self.number {
                        3 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Minor),
                        5 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),
                        6 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        7 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Minor),
                        11 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),
                        _ => return Err(()),
                    },
                    // TODO: Not all EDOs have a mid quality
                    MacroAlteration::Mid => match self.number {
                        3 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Mid),
                        5 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),
                        6 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Mid),
                        7 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Mid),
                        11 => self.quality = IntervalQuality::Perfect(PerfectQuality::Mid),
                        _ => return Err(()),
                    },
                    MacroAlteration::Major => match self.number {
                        3 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        5 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),
                        6 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        7 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        11 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),
                        _ => return Err(()),
                    },
                    MacroAlteration::Augmented => match self.number {
                        3 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        5 => self.quality = IntervalQuality::Perfect(PerfectQuality::Augmented),
                        6 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        7 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Minor),
                        11 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),
                        _ => return Err(()),
                    },
                    MacroAlteration::Diminished => match self.number {
                        3 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        5 => self.quality = IntervalQuality::Perfect(PerfectQuality::Diminished),
                        6 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Major),
                        7 => self.quality = IntervalQuality::Imperfect(ImperfectQuality::Minor),
                        11 => self.quality = IntervalQuality::Perfect(PerfectQuality::Perfect),
                        _ => return Err(()),
                    },
                    // Default to false since intervals that don't match need alterations, which is a better default
                    _ => return Err(()),
                }
            }
        }
        Ok(())
    }
    /// Check if this interval matches a "main" alteration, the alteration of a chord
    pub fn matches_main_alteration(&self, macro_alteration: &str, micro_alteration: i32) -> bool {
        if !(self.number == 3 || self.number == 6 || self.number == 7 || self.number == 11) {
            // main micro alterations only apply to intervals number 3, 6, 7 and 11
            if micro_alteration != self.deviation {
                return false;
            }
        }

        // Intervals that don't occur in chords always match
        if self.number == 1
            || self.number == 8
            || self.number > 13
            || (self.number > 7 && self.number % 2 == 0)
        {
            return true;
        }

        // Certain intervals aren't affected by the macro alteration and only match if they are at their default
        match self.number {
            4 => matches!(
                self.quality,
                IntervalQuality::Perfect(PerfectQuality::Perfect)
            ),
            13 => matches!(
                self.quality,
                IntervalQuality::Imperfect(ImperfectQuality::Major)
            ),
            2 | 9 => matches!(
                self.quality,
                IntervalQuality::Imperfect(ImperfectQuality::Major)
            ),
            _ => {
                // TODO: Thesse only work for diatonic EDOs
                match macro_alteration {
                    "" => match self.number {
                        3 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        5 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Perfect)
                        ),
                        6 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        7 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Minor)
                        ),
                        11 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Perfect)
                        ),
                        _ => false,
                    },
                    "m" => match self.number {
                        3 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Minor)
                        ),
                        5 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Perfect)
                        ),
                        6 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        7 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Minor)
                        ),
                        11 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Perfect)
                        ),
                        _ => false,
                    },
                    "~" => match self.number {
                        3 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Mid)
                        ),
                        5 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Perfect)
                        ),
                        6 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Mid)
                        ),
                        7 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Mid)
                        ),
                        11 => matches!(self.quality, IntervalQuality::Perfect(PerfectQuality::Mid)),
                        _ => false,
                    },
                    "M" => match self.number {
                        3 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        5 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Perfect)
                        ),
                        6 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        7 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        11 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Perfect)
                        ),
                        _ => false,
                    },
                    "a" => match self.number {
                        3 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        5 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Augmented)
                        ),
                        6 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        7 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Minor)
                        ),
                        11 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Perfect)
                        ),
                        _ => false,
                    },
                    "d" => match self.number {
                        3 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        5 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Diminished)
                        ),
                        6 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Major)
                        ),
                        7 => matches!(
                            self.quality,
                            IntervalQuality::Imperfect(ImperfectQuality::Minor)
                        ),
                        11 => matches!(
                            self.quality,
                            IntervalQuality::Perfect(PerfectQuality::Perfect)
                        ),
                        _ => false,
                    },
                    // Default to false since intervals that don't match need alterations, which is a better default
                    _ => false,
                }
            }
        }
    }
    pub fn to_degree(&self) -> i32 {
        match self.quality {
            IntervalQuality::Perfect(perfect_quality) => {
                let base_degree = edo_n_interval_number_to_degrees(self.edo, self.number) as i32;
                let sharp_n = edo_n_sharp_degrees(self.edo).expect("Unsupported EDO") as i32;
                // double the alteration and then divide by two to support mid values
                let major_alteration = (match perfect_quality {
                    PerfectQuality::Augmented => 2,
                    PerfectQuality::Perfect => 0,
                    // TODO: This only works if there is indeed a value halfway inbetween, which is not true for all EDOs that use the mid symbol.
                    PerfectQuality::Mid => match self.number {
                        4 => 1,
                        5 => -1,
                        _ => panic!("Unsupported mid interval used!"),
                    },
                    PerfectQuality::Diminished => -2,
                } * sharp_n)
                    / 2;
                base_degree + major_alteration + self.deviation
            }
            IntervalQuality::Imperfect(imperfect_quality) => {
                let base_degree = edo_n_interval_number_to_degrees(self.edo, self.number) as i32;
                let sharp_n = edo_n_sharp_degrees(self.edo).expect("Unsupported EDO") as i32;
                let major_alteration = match imperfect_quality {
                    ImperfectQuality::Augmented => 1,
                    ImperfectQuality::Major => 0,
                    ImperfectQuality::Mid => todo!(),
                    ImperfectQuality::Minor => -1,
                    ImperfectQuality::Diminished => -2,
                } * sharp_n;
                base_degree + major_alteration + self.deviation
            }
        }
    }
    pub fn to_edo_interval(&self) -> EdoInterval {
        EdoInterval::new(self.to_degree(), self.edo)
    }
    // fn get_deviation_text(&self, use_mid: bool) -> String {
    //     match &self.kind {
    //         EdoIntervalSemanticKind::Perfect {
    //             degree_perfect,
    //             degree,
    //         } => {
    //             let deviation = degree - degree_perfect;
    //             if deviation == 0 {
    //                 "P".to_owned()
    //             } else {
    //                 // TODO: Support mid values. Mid occurs halfway between perfect and augmented or diminished. ~4 is halfway augmented while ~5 is halfway diminished.
    //                 let sharp_n = edo_n_sharp_degrees(self.divisions).expect("Not able to get the value of a sharp for this edo") as i32;
    //                 // get the closest alteration (perfect, augmented, dminished)
    //                 let (main_alteration, minor_alteration) = if sharp_n > 0 {
    //                     let main_alteration = (deviation as f32 / sharp_n as f32).round() as i32;
    //                     let minor_alteration = deviation - (main_alteration * sharp_n);
    //                     (main_alteration, minor_alteration)
    //                 } else {
    //                     // there are sharp-0 EDOs e.g. 7edo and 14edo
    //                     // sharp-0, all alterations are minor e.g. ups or downs
    //                     (0, deviation)
    //                 };
    //                 let result = String::new();
    //                 let main_type = if main_alteration == 0 {
    //                     'P'
    //                 } else if main_alteration > 0 {
    //                     'A'
    //                 } else {
    //                     'D'
    //                 };
    //                 let minor_type = if minor_alteration > 0 {
    //                     Some('^')
    //                 } else if minor_alteration < 0 {
    //                     Some('v')
    //                 } else {
    //                     None
    //                 };
    //                 for _ in 0..minor_alteration.abs() {
    //                     assert!(minor_type.is_some());
    //                     result.push(minor_type.unwrap());
    //                 }
    //                 result.push(main_type);
    //                 result
    //             }
    //         }
    //         EdoIntervalSemanticKind::Imperfect {
    //             degree_major,
    //             degree_minor,
    //             degree,
    //         } => {
    //             // There are no sharp-0 imperfect intervals.
    //             // possible values are minor, major or mid (m, M, ~) + ups or downs.
    //             // Mid values occur in between minor and major in many EDOs, but sharp-0, sharp-1 and sharp-3 EDOs have no mid intervals.
    //             // Given N = sharp_N, there are N-1 steps between minor and Major.
    //             // sharp-2: m ~ M
    //             // sharp-4 to sharp-6 follow the pattern m ^m [ mid values ] vM M
    //             // sharp-7 to sharp-10 follow the pattern m ^m ^^m [ mid values ] vvM vM M
    //             //
    //             let sharp_n = edo_n_sharp_degrees(self.divisions).expect("Not able to get the value of a sharp for this edo") as i32;
    //             if sharp_n == 0 {
    //                 panic!("Error: There are no sharp-0 imperfect intervals.");
    //             }
    //             // Determine the number of steps in the EDO that are still considered part of each respective quality.
    //             // minor/major are symmetrical in all EDOs.
    //             let steps_minor_major = match sharp_n {
    //                 1 | 2 => 1,
    //                 3..=6 => 2,
    //                 7..=10 => 3,
    //             };
    //             let steps_mid = match sharp_n {
    //                 1 | 3 => 0,
    //                 _ => (sharp_n + 1) - (steps_minor_major * 2),
    //             };
    //             // if steps_mid is odd there is an unaltered ~ value in the middle
    //             let deviation = degree - degree_minor;
    //             let imperfect_kind = if deviation < steps_minor_major {
    //                 ImperfectKind::Minor
    //             } else if deviation >= steps_minor_major
    //                 && deviation < (steps_minor_major + steps_mid)
    //             {
    //                 ImperfectKind::Mid
    //             } else {
    //                 ImperfectKind::Major
    //             };
    //             if (degree_major - degree).abs() < (degree_minor - degree).abs() {
    //                 // major
    //             }
    //         }
    //     }
    // }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum ImperfectQuality {
    Augmented,
    Major,
    Mid,
    Minor,
    Diminished,
}
impl ImperfectQuality {
    pub fn apply_macro_alteration(&mut self, ma: MacroAlteration) {
        match ma {
            MacroAlteration::None => (),
            MacroAlteration::Diminished => *self = ImperfectQuality::Diminished,
            MacroAlteration::Augmented => *self = ImperfectQuality::Augmented,
            MacroAlteration::Minor => *self = ImperfectQuality::Minor,
            MacroAlteration::Mid => *self = ImperfectQuality::Mid,
            MacroAlteration::Major => *self = ImperfectQuality::Major,
        }
    }
}

/// TODO: Support the mid quality for perfect intervals
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum PerfectQuality {
    Augmented,
    Perfect,
    /// NB: a perfect quality `Mid` interval can be either lower (5th) or higher (4th) than perfect. I.e. midway between perfect and diminished or perfect and augmented.
    Mid,
    Diminished,
}
impl PerfectQuality {
    pub fn apply_macro_alteration(&mut self, ma: MacroAlteration) {
        match ma {
            MacroAlteration::None | MacroAlteration::Major | MacroAlteration::Minor => (),
            MacroAlteration::Diminished => *self = PerfectQuality::Diminished,
            MacroAlteration::Augmented => *self = PerfectQuality::Augmented,
            MacroAlteration::Mid => *self = PerfectQuality::Mid,
        }
    }
}

// TODO: Proper error
/// Map a note letter name such as "C" or "G" to a degree in a specific EDO. Assumes the letter names correspond to stacked fifths. Will be a positive degree number where C is 0.
fn edo_n_letter_to_degrees(edo: u32, letter: char) -> Result<i32, ()> {
    let num_fifths = match letter {
        'F' => -1,
        'C' => 0,
        'G' => 1,
        'D' => 2,
        'A' => 3,
        'E' => 4,
        'B' => 5,
        _ => return Err(()),
    };

    let degrees_per_fifth = edo_n_interval_number_to_degrees(edo, 5);
    let degree = ((degrees_per_fifth as i32 * num_fifths) + edo as i32) as u32 % edo;

    Ok(degree as i32)
}

/// Returns how many degrees a sharp or a flat changes the pitch in an EDO
fn edo_n_sharp_degrees(edo: u32) -> Option<u32> {
    match edo {
        7 | 14 | 21 | 28 | 35 => Some(0),
        5 | 9 | 12 | 16 | 19 | 23 | 26 | 33 | 40 | 47 => Some(1),
        10 | 11 | 17 | 18 | 24 | 31 | 38 | 45 | 52 => Some(2),
        13 | 15 | 22 | 29 | 36 | 43 | 50 | 57 | 64 => Some(3),
        20 | 27 | 34 | 41 | 48 | 55 | 62 | 69 => Some(4),
        25 | 32 | 39 | 46 | 53 | 60 | 67 => Some(5),
        30 | 37 | 44 | 51 | 58 | 65 | 72 => Some(6),
        42 | 49 | 56 | 63 | 70 => Some(7),
        54 | 61 | 68 => Some(8),
        59 | 66 => Some(9),
        71 => Some(10),
        _ => {
            // If the edo is a multiple of some other edo that is in the list we can calculate the size of a sharp
            if edo % 2 == 0 {
                edo_n_sharp_degrees(edo / 2).map(|sharp_n| sharp_n * 2)
            } else if edo % 3 == 0 {
                edo_n_sharp_degrees(edo / 3).map(|sharp_n| sharp_n * 3)
            } else {
                None
            }
        }
    }
}
/// Returns the minor second value for an EDO. Using this together with the
/// edo_n_sharp_degrees function, a diatonic scale can be calculated for any
/// diatonic EDO. If multiple options are possible, the one corresponding to a
/// diatonic interpretation of the EDO is chosen.
fn edo_n_minor_second(edo: u32) -> Option<i32> {
    match edo {
        7 | 12 | 17 | 22 | 27 | 32 | 37 | 42 => Some(1),
        9 | 14 | 19 | 24 | 29 | 34 | 39 | 44 | 49 => Some(2),
        11 | 16 | 21 | 26 | 31 | 36 | 41 | 46 | 51 => Some(3),
        23 | 33 | 38 | 43 | 48 | 53 => Some(4),
        35 | 40 | 45 | 50 => Some(5),
        47 | 52 => Some(6),
        _ => {
            // If the edo is a multiple of some other edo that is in the list we can calculate the size of a sharp
            if edo % 2 == 0 {
                edo_n_minor_second(edo / 2).map(|m2| m2 * 2)
            } else if edo % 3 == 0 {
                edo_n_minor_second(edo / 3).map(|m2| m2 * 3)
            } else {
                None
            }
        }
    }
}
/// For a given EDO, return the number of degrees for the base version of the
/// interval with the specified number. For perfect intervals, the degree for
/// the perfect quality returned. For imperfect intervals, the degree for the
/// major quality is returned.
fn edo_n_interval_number_to_degrees(edo: u32, number: u32) -> u32 {
    let edo_category = EdoCategory::from_edo(edo);
    match edo_category {
        EdoCategory::Perfect => {
            // perfect EDOs are multiples of 7
            (number - 1) * (edo / 7)
        }
        EdoCategory::Diatonic => {
            // A major scale, containing all the basic intervals, can be constructed in any EDO using two step sizes. big_step = sharp_n + small_step.
            let sharp_n = edo_n_sharp_degrees(edo).expect("Unsupported EDO");
            let n = edo_n_minor_second(edo).expect("Unsupported EDO") as u32; //narrow
            let w = n + sharp_n; //wide
            let mut degree = 0;
            for i in 1..number {
                degree += match (i - 1) % 7 {
                    2 | 6 => n,
                    _ => w,
                };
            }
            degree
        }
        EdoCategory::Pentatonic => todo!(),
        EdoCategory::Supersharp => todo!(),
        EdoCategory::Trivial => todo!(),
        EdoCategory::Superflat => todo!(),
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum EdoCategory {
    Superflat,
    Perfect,
    Diatonic,
    Pentatonic,
    Supersharp,
    Trivial,
}
impl EdoCategory {
    /// Returns the EdoCategory given a particular EDO.
    /// Note: 13 and 18 EDO can be consider either superflat or supersharp. This function considers them superflat since the supersharp category is IMHO the most unintuitive one.
    pub fn from_edo(edo: u32) -> Self {
        match edo {
            2 | 3 | 4 | 6 => EdoCategory::Trivial,
            9 | 11 | 13 | 16 | 18 | 23 => EdoCategory::Superflat,
            8 => EdoCategory::Supersharp,
            7 | 14 | 21 | 28 | 35 => EdoCategory::Perfect,
            _ => {
                if edo % 5 == 0 {
                    EdoCategory::Pentatonic
                } else {
                    EdoCategory::Diatonic
                }
            }
        }
    }
}

pub enum ChordNoteSelector {
    /// The bass note of the chord (e.g. the root, the 3rd etc.)
    Bass,
    /// Get the root note of the chord
    Root,
    /// Get the Nth note from the top
    NFromTop(usize),
    /// Get the Nth note from the bottom
    NFromBottom(usize),
    /// Get the note in the chord nearest to the given absolute pitch
    NearestPitch(EdoPitch),
    /// Get the note in the chord nearest the given interval
    NearestInterval(EdoInterval),
}

#[derive(Error, Debug)]
pub enum PitchError {
    #[error("The pitch or interval name `{0}` cannot be found in this context.")]
    NameCannotBeFound(String),
}

#[cfg(test)]
mod test {
    use crate::pitch::{
        edo_n_minor_second, edo_n_sharp_degrees, EdoChordSemantic, EdoInterval,
        EdoIntervalSemantic, EdoPitch, ImperfectQuality, IntervalQuality,
    };

    #[test]
    fn edo_categories() {
        use crate::pitch::EdoCategory;
        assert_eq!(EdoCategory::from_edo(53), EdoCategory::Diatonic);
        assert_eq!(EdoCategory::from_edo(19), EdoCategory::Diatonic);
        assert_eq!(EdoCategory::from_edo(31), EdoCategory::Diatonic);
        assert_eq!(EdoCategory::from_edo(12), EdoCategory::Diatonic);
        assert_eq!(EdoCategory::from_edo(41), EdoCategory::Diatonic);
        assert_eq!(EdoCategory::from_edo(6), EdoCategory::Trivial);
    }
    #[test]
    fn edo_sharp_n() {
        assert_eq!(edo_n_sharp_degrees(41), Some(4));
        assert_eq!(edo_n_sharp_degrees(31), Some(2));
        assert_eq!(edo_n_sharp_degrees(53), Some(5));
        assert_eq!(edo_n_sharp_degrees(12), Some(1));
    }
    #[test]
    fn edo_53_interval_numbers() {
        use crate::pitch::edo_n_interval_number_to_degrees;
        assert_eq!(edo_n_interval_number_to_degrees(53, 2), 9);
        assert_eq!(edo_n_interval_number_to_degrees(53, 3), 18);
        assert_eq!(edo_n_interval_number_to_degrees(53, 4), 22);
        assert_eq!(edo_n_interval_number_to_degrees(53, 5), 31);
        assert_eq!(edo_n_interval_number_to_degrees(53, 6), 40);
        assert_eq!(edo_n_interval_number_to_degrees(53, 7), 49);
        assert_eq!(edo_n_interval_number_to_degrees(53, 8), 53);
        assert_eq!(edo_n_interval_number_to_degrees(53, 9), 62);
    }
    #[test]
    fn edo_31_interval_numbers() {
        use crate::pitch::edo_n_interval_number_to_degrees;
        assert_eq!(edo_n_interval_number_to_degrees(31, 2), 5);
        assert_eq!(edo_n_interval_number_to_degrees(31, 3), 10);
        assert_eq!(edo_n_interval_number_to_degrees(31, 4), 13);
        assert_eq!(edo_n_interval_number_to_degrees(31, 5), 18);
        assert_eq!(edo_n_interval_number_to_degrees(31, 6), 23);
        assert_eq!(edo_n_interval_number_to_degrees(31, 7), 28);
        assert_eq!(edo_n_interval_number_to_degrees(31, 8), 31);
        assert_eq!(edo_n_interval_number_to_degrees(31, 9), 36);
    }
    #[test]
    fn minor_second_for_edos() {
        assert_eq!(edo_n_minor_second(12), Some(1));
        assert_eq!(edo_n_minor_second(31), Some(3));
        assert_eq!(edo_n_minor_second(53), Some(4));
        assert_eq!(edo_n_minor_second(159), Some(4 * 3));
        assert_eq!(edo_n_minor_second(144), Some(6 * 2));
    }
    #[test]
    fn simple_chords() {
        let mut chord = EdoChordSemantic::from_root_major(EdoPitch::new(0, 53));
        assert_eq!(chord.to_chord_symbol(), "C".to_owned());
        chord.set_interval(EdoIntervalSemantic {
            quality: IntervalQuality::Imperfect(ImperfectQuality::Minor),
            deviation: 0,
            edo: 53,
            number: 3,
        });
        assert_eq!(chord.to_chord_symbol(), "Cm".to_owned());
        chord.set_interval(EdoIntervalSemantic {
            quality: IntervalQuality::Imperfect(ImperfectQuality::Minor),
            deviation: 1,
            edo: 53,
            number: 3,
        });
        assert_eq!(chord.to_chord_symbol(), "C^m".to_owned());
    }
    #[test]
    fn equality() {
        assert_eq!(EdoInterval::new(4, 4), EdoInterval::new(2, 2));
        assert_eq!(EdoInterval::new(10, 12), EdoInterval::new(20, 24));
        assert_eq!(EdoInterval::new(10, 12), EdoInterval::new(60, 72));
    }
}

/*
pitch() attached to the PitchSystem, each PitchSystem being a different type with a trait for referencing other PitchSystems

edo.pitch(17);
edo.pitch(17.5);
edo.pitch_all(vec![17, 14, 31, 39, 0]); // -> PitchSequence<EdoPitch>
ji.pitch(4, 7);

type PitchSystemReference = Arc<RwLock<dyn PitchSystem>>

one helper function per type
edo_pitch(17, pitch_system);
edo_pitch_fract(17.9, pitch_system);
edo_pitch_all(vec![17, 14, 31, 39, 0], pitch_system); // -> PitchSequence<EdoPitch>
ji_pitch(4, 7, pitch_system);
*/
