// harmonic network

// A network of harmonic motion can be designed from the lattices in Harmonic Experience

// A unit of the lattice consists of both the major and minor versions of chords a 5th apart, e.g. C F Bb
// Any chord can move by fifths up or down, preserving its major/minor mode. It can also move by 2 5ths if you immediately go back to the chord in between e.g. F G C.

// A major chord can move down the lattice to the parallel chords of the chords in the some 5th row (e.g. C -> fm/cm/gm)
// or up the lattice to a minor chord m3 below or M3 above (e.g. C -> am/em).

// A minor chord can move to the parallel chords like the major ones, and it can move down the lattice to chords in major M3 below or m3 above.

// A list of possible chords can be created this way, and they can be weighted by probability. Some chord changes should be less common than others (e.g. C -> cm and am -> C).

// We could also add some "gravity" to return back to the starting chord eventually.
//

use crate::pitch::{EdoChord, EdoPitch};
use rand::distributions::WeightedIndex;
use rand::prelude::*;
use std::collections::HashSet;

#[derive(Debug, Clone)]
pub struct HarmonicNetwork {
    nodes: Vec<HarmonicNode>,
    edges: Vec<HarmonicEdge>,
    current_node: usize,
    center_node_i: usize,
    /// how many times we've stepped to a new chord since we were last at the center chord
    steps_from_center: usize,
}

impl HarmonicNetwork {
    pub fn new(root: EdoPitch, depth: usize) -> Self {
        // Calculate all chords we need
        let mut chord_set = HashSet::new();
        let mut stack = Vec::new();
        stack.push(EdoChord::from_relative_degrees(root, &[0, 17, 31]));
        for _i in 0..depth {
            let mut new_stack = Vec::new();
            for chord in stack {
                let mut adjacent: Vec<EdoChord> = get_weighted_adjacent_chords(&chord)
                    .into_iter()
                    .map(|p| p.0)
                    .collect();
                chord_set.insert(chord);
                new_stack.append(&mut adjacent);
            }
            stack = new_stack;
        }
        // Convert chords to nodes
        let mut nodes = Vec::new();
        for chord in chord_set.drain() {
            nodes.push(HarmonicNode::new(chord));
        }
        // Add edges to all nodes
        let mut edges = Vec::new();
        for index in 0..nodes.len() {
            let chord = &nodes[index].chord;
            let adjacent = get_weighted_adjacent_chords(&chord);
            for (chord, weight) in adjacent {
                let adjacent_index = nodes.iter().enumerate().find(|&node| node.1.chord == chord);
                if let Some((i, _n)) = adjacent_index {
                    edges.push(HarmonicEdge::new(i, index, weight));
                }
            }
        }
        // Calculate the shortest distance to the center from each node
        // Go through all edges pointing to a node and update them whenever a shorter path is found to that node.
        // Vec<(node_index, distance_to_node)>
        let mut nodes_to_update: Vec<(usize, f32)> = Vec::new();
        let start_distance = 0.0;
        // Find the start node index
        let mut center_node_i = 0;
        for (i, node) in nodes.iter().enumerate() {
            if node.chord.root == root && node.chord.is_major() {
                center_node_i = i;
                break;
            }
        }
        let edges_to_start = get_edges_to_node(center_node_i, &edges);
        for edge_i in edges_to_start {
            let edge_distance = start_distance + edges[edge_i].distance_weight();
            if edges[edge_i].distance_to_center.unwrap_or(f32::MAX) >= edge_distance {
                edges[edge_i].distance_to_center = Some(edge_distance);
                nodes_to_update.push((edges[edge_i].source_i, edge_distance));
            }
        }
        while nodes_to_update.len() > 0 {
            let (node_index, accumulated_distance) = nodes_to_update.remove(0);
            let edges_to_node = get_edges_to_node(node_index, &edges);
            for edge_i in edges_to_node {
                let edge_distance = accumulated_distance + edges[edge_i].distance_weight();
                // If we have found a shorter path to the center from the node, update the edge and push the node to be updated later
                if edges[edge_i].distance_to_center.unwrap_or(f32::MAX) >= edge_distance {
                    edges[edge_i].distance_to_center = Some(edge_distance);
                    nodes_to_update.push((edges[edge_i].source_i, edge_distance));
                }
            }
        }

        // Calculate how much closer to the center one edge is compared to all the other edges from the same node
        for (i, node) in nodes.iter().enumerate() {
            let edges_from_node = get_edges_from_node(i, &edges);
            if edges_from_node.len() > 1 {
                let mut distance_sum = 0.0;
                for edge_i in &edges_from_node {
                    distance_sum += edges[*edge_i].distance_to_center.unwrap();
                }
                let mean = distance_sum / edges_from_node.len() as f32;
                for edge_i in &edges_from_node {
                    let distance_to_center = edges[*edge_i].distance_to_center.unwrap();
                    // The relative_distance_weight should be reversely proportional to
                    edges[*edge_i].relative_distance_weight =
                        Some(((distance_to_center - mean) / mean) * -1.0);
                }
                // Shift so that the lowest relative_distance_weight is at 0
                let lowest_rel = edges_from_node
                    .iter()
                    .map(|&i| edges[i].relative_distance_weight.unwrap())
                    .fold(f32::INFINITY, |a, b| a.min(b));
                let sum = edges_from_node
                    .iter()
                    .map(|&i| edges[i].relative_distance_weight.unwrap())
                    .sum::<f32>()
                    - (lowest_rel * edges_from_node.len() as f32);
                for edge_i in &edges_from_node {
                    if let Some(ref mut d) = edges[*edge_i].relative_distance_weight {
                        *d -= lowest_rel; // make positive and have 0 as lowest
                        *d /= sum; // normalize the sum of all relative weigts combined
                    }
                }
            } else {
                // If there's only one edge, it is the same distance as itself
                // If there's no edge (which should be impossible) the iterator notation will save us
                for edge_i in edges_from_node {
                    edges[edge_i].relative_distance_weight = Some(1.0);
                }
            }
        }

        for edge in &edges {
            if edge.source_i == edge.destination_i {
                eprintln!("edge pointing to itself");
            }
            if edge.base_weight.is_nan() {
                eprintln!("edge base weight is nan: {:#?}", edge);
            }
            if edge.relative_distance_weight.unwrap().is_nan() {
                eprintln!("edge relative distance weight is nan: {:#?}", edge);
            }
            if edge.distance_to_center.unwrap().is_nan() {
                eprintln!("edge distance to center is nan: {:#?}", edge);
            }
        }

        Self {
            nodes,
            edges,
            current_node: center_node_i,
            center_node_i,
            steps_from_center: 0,
        }
    }

    pub fn get_next_chord(&mut self) -> EdoChord {
        let edges_from_node = get_edges_from_node(self.current_node, &self.edges);
        let relative_distances: Vec<f32> = edges_from_node
            .iter()
            .map(|&i| self.edges[i].relative_distance_weight.unwrap())
            .collect();
        let base_weights: Vec<f32> = edges_from_node
            .iter()
            .map(|&i| self.edges[i].base_weight)
            .collect();
        let node_indices: Vec<usize> = edges_from_node
            .iter()
            .map(|&i| self.edges[i].destination_i)
            .collect();

        // make distance more discriminant (larger distance between good and bad) when a higher number of steps away from the center
        let relative_distances: Vec<f32> = relative_distances
            .iter()
            .map(|&d| d.powf((self.steps_from_center as f32).powf(0.25)))
            .collect();
        // normalize
        let mut sum: f32 = relative_distances.iter().sum();
        // avoid a division by zero
        if sum == 0.0 {
            sum = 1.0;
        }
        let relative_distances: Vec<f32> = relative_distances.iter().map(|&d| d / sum).collect();
        // make the distance worth more relative to the base_weight when far from the center
        let relative_distances: Vec<f32> = relative_distances
            .iter()
            .map(|&d| d * (self.steps_from_center as f32).powf(0.5))
            .collect();
        let combined: Vec<f32> = relative_distances
            .iter()
            .zip(base_weights.iter())
            .map(|(&r, &b)| r + b)
            .collect();

        // println!("probabilities: ");
        // for (node, weight) in node_indices.iter().zip(combined.iter()) {
        //     println!(
        //         "{} {:?}: {}",
        //         self.nodes[*node].chord.root.degree,
        //         self.nodes[*node].chord.is_major(),
        //         weight
        //     );
        // }

        let dist = WeightedIndex::new(&combined).unwrap();
        let mut rng = thread_rng();
        let node_i = node_indices[dist.sample(&mut rng)];

        if node_i == self.center_node_i {
            self.steps_from_center = 0;
        } else {
            self.steps_from_center += 1;
        }
        self.current_node = node_i;

        self.nodes[node_i].chord.clone()
    }

    pub fn print_debug(&self) {
        for (i, node) in self.nodes.iter().enumerate() {
            println!("root: {}", node.chord.root.degree);
            println!("edges to node: ");
            let edges_to_node = get_edges_to_node(i, &self.edges);
            for edge_i in edges_to_node {
                println!("{:#?}", self.edges[edge_i]);
            }
            println!("\n");
        }
    }
}

#[derive(Debug, Clone)]
struct HarmonicNode {
    chord: EdoChord,
}

impl HarmonicNode {
    pub fn new(chord: EdoChord) -> Self {
        Self { chord }
    }
}

/// A directed edge from source_i to destination_i
#[derive(Debug, Clone)]
struct HarmonicEdge {
    source_i: usize,
    destination_i: usize,
    base_weight: f32,
    distance_to_center: Option<f32>,
    relative_distance_weight: Option<f32>,
    combined_weight: Option<f32>,
}
impl HarmonicEdge {
    pub fn new(source_i: usize, destination_i: usize, weight: f32) -> Self {
        Self {
            source_i,
            destination_i,
            base_weight: weight,
            distance_to_center: None,
            relative_distance_weight: None,
            combined_weight: None,
        }
    }

    pub fn distance_weight(&self) -> f32 {
        1.0 / self.base_weight
    }
}
fn get_weighted_adjacent_chords(chord: &EdoChord) -> Vec<(EdoChord, f32)> {
    let mut adjacent = Vec::new();
    let third = if chord.is_major() { 17 } else { 14 };
    let opposite_third = 31 - third;
    // 5ths with the same mode
    adjacent.push((
        EdoChord::from_relative_degrees(chord.root.from_add_degree(-31), &[0, third, 31]),
        4.,
    ));
    adjacent.push((
        EdoChord::from_relative_degrees(chord.root.from_add_degree(31), &[0, third, 31]),
        4.,
    ));
    // pentamerously parallel chords
    adjacent.push((
        EdoChord::from_relative_degrees(chord.root.from_add_degree(-31), &[0, opposite_third, 31]),
        1.,
    ));
    adjacent.push((
        EdoChord::from_relative_degrees(chord.root.from_add_degree(0), &[0, opposite_third, 31]),
        0.3,
    ));
    adjacent.push((
        EdoChord::from_relative_degrees(chord.root.from_add_degree(31), &[0, opposite_third, 31]),
        1.,
    ));
    // pentamerous movement
    // mediant chords within the key of the root
    adjacent.push((
        EdoChord::from_relative_degrees(
            chord.root.from_add_degree(third),
            &[0, opposite_third, 31],
        ),
        0.7,
    ));
    adjacent.push((
        EdoChord::from_relative_degrees(
            chord.root.from_add_degree(opposite_third * -1),
            &[0, opposite_third, 31],
        ),
        0.7,
    ));

    // normalize the sum of the weights
    let weight_sum: f32 = adjacent.iter().map(|p| p.1).sum();
    for pair in adjacent.iter_mut() {
        pair.1 /= weight_sum;
    }

    adjacent
}

fn get_edges_to_node(i: usize, edges: &Vec<HarmonicEdge>) -> Vec<usize> {
    let mut result = Vec::new();
    for (edge_i, edge) in edges.iter().enumerate() {
        if edge.destination_i == i {
            result.push(edge_i);
        }
    }
    result
}

fn get_edges_from_node(i: usize, edges: &Vec<HarmonicEdge>) -> Vec<usize> {
    let mut result = Vec::new();
    for (edge_i, edge) in edges.iter().enumerate() {
        if edge.source_i == i {
            result.push(edge_i);
        }
    }
    result
}
