use fraction::Bounded;
use fraction::Fraction;
use fraction::ToPrimitive;
use fraction::Zero;
use lilypond_pargen::lp_node::*;

#[derive(Clone, Debug)]
pub struct Bar {
    numerator: u32,
    denominator: u32,
    /// Groupings are how the beats based on the denominator are grouped in the bar e.g. 3 + 4, 3 + 3 + 3, 2 + 2
    groupings: Vec<u32>,
}
impl PartialEq for Bar {
    fn eq(&self, other: &Self) -> bool {
        if self.groupings.len() != other.groupings.len() {
            return false;
        }
        let mut same_groupings = true;
        for (sg, og) in self.groupings.iter().zip(other.groupings.iter()) {
            if sg != og {
                return false;
            }
        }
        self.numerator == other.numerator && self.denominator == other.denominator
    }
}

impl Bar {
    pub fn new(numerator: u32, denominator: u32) -> Self {
        let groupings = Self::default_groupings(numerator, denominator);
        Self {
            numerator,
            denominator,
            groupings,
        }
    }
    pub fn zero() -> Self {
        Self::new(0, 0)
    }
    fn default_groupings(numerator: u32, denominator: u32) -> Vec<u32> {
        match numerator {
            9 => vec![3, 3, 3],
            12 => vec![4, 4, 4],
            _ => {
                if numerator % 2 == 0 {
                    // For even numerators, default to half bar groupings
                    vec![numerator / 2, numerator / 2]
                } else {
                    // Otherwise, default to a longer first half i.e. 3+2, 4+3 etc
                    vec![(numerator / 2) + 1, numerator / 2]
                }
            }
        }
    }
    /// Change the time signature to match the duration given
    pub fn set_duration(&mut self, duration: Fraction) {
        self.numerator = *duration.numer().unwrap() as u32;
        self.denominator = *duration.denom().unwrap() as u32;
        while self.denominator < 4 {
            self.numerator *= 2;
            self.denominator *= 2;
        }
        self.groupings = Bar::default_groupings(self.numerator, self.denominator);
    }
    pub fn to_fraction(&self) -> Fraction {
        Fraction::new(self.numerator, self.denominator)
    }
    pub fn to_lilypond(&self) -> LPNode {
        LPNode::time_sig(self.numerator, self.denominator)
    }
    pub fn to_time_signature(&self) -> TimeSignature {
        TimeSignature::from(self.numerator, self.denominator)
    }
}
pub type Beat = Fraction;

#[derive(Clone, Debug)]
pub struct Marker {
    id: i64,
    description: String,
    beat_position: Beat,
}

/// A way to define a temporal grid that acts as an overlay on top of
/// beats to determine the time signatures and the tempos throughout
/// a section.
///
/// If the TemporalGrid is shorter in time than the material it is
/// applied to, the last full bar is repeated indefinitely.
#[derive(Clone, Debug)]
pub struct TemporalGrid {
    bars: Vec<Bar>,
    tempo_events: Vec<TempoEvent>,
    pub bpm: Option<f32>, // A stand in untiil we have tempo automation lanes
    //
    // tempo_lanes: Vec<Envelope<Beat>>, // TODO
    /// Markers are useful to reference a specific temporal spot when
    /// there might be time added before or after that spot. The
    /// marker will stay with any musical material defined at that
    /// point. Perhaps better implemented as a MusicStream?
    markers: Vec<Marker>,
}

impl TemporalGrid {
    pub fn from_bars(bars: Vec<Bar>) -> Self {
        Self {
            bars,
            bpm: None,
            tempo_events: vec![],
            markers: vec![],
        }
    }
    pub fn from_constant_bar_length(bar: Bar) -> Self {
        Self {
            bars: vec![bar],
            bpm: None,
            tempo_events: vec![],
            markers: vec![],
        }
    }
    pub fn push_bar(&mut self, bar: Bar) {
        self.bars.push(bar);
    }

    pub fn get_total_position_at_bar_index(&self, index: usize) -> Beat {
        if index <= self.bars.len() {
            self.bars[0..index]
                .iter()
                .fold(Fraction::zero(), |sum, bar| sum + bar.to_fraction())
        } else {
            panic!("Trying to access bar with index out of bounds!");
        }
    }
    pub fn get_tempo_event_index_at_position(&self, position: Fraction) -> Option<usize> {
        let mut pos_pointer = Fraction::zero();
        for (i, event) in self.tempo_events.iter().enumerate() {
            if position >= pos_pointer && position < pos_pointer + event.duration {
                return Some(i);
            }
            pos_pointer += event.duration;
        }
        None
    }
    pub fn insert_bpm<T: AsRef<str>>(&mut self, bpm: f32, text: T, position: Fraction) {
        println!("Insert bpm {} at position {}", bpm, position);
        self.insert_tempo_event(TempoEvent::bpm(bpm, text.as_ref().to_owned()), position);
    }
    pub fn insert_tempo_event(&mut self, tempo_event: TempoEvent, position: Fraction) -> &mut Self {
        // If the position is exactly at a duration, insert the new
        // values at that index and then remove `duration` worth of
        // values afterwards.

        if position < self.total_tempo_event_duration() {
            let pos_index = self.get_tempo_event_index_at_position(position);
            if let Some(mut pos_index) = pos_index {
                let index_pos = self.get_total_position_at_bar_index(pos_index);
                if index_pos < position {
                    // The `position` is in the middle of an event.
                    // Shorten the value at the position and jump forward one index
                    let new_duration = position - index_pos;
                    self.tempo_events[pos_index].duration = new_duration;
                    pos_index += 1; // Insert after this pitch
                }
                // Save the duration of the event for the future
                let mut duration_remaining = tempo_event.duration;
                // Insert the new value
                self.tempo_events.insert(pos_index, tempo_event);

                // Check if there are any later values to shorten/remove
                if pos_index < self.tempo_events.len() - 1 {
                    // Remove and potentially shorten values after this one

                    pos_index += 1; // Look at indices after the inserted note
                    loop {
                        let dur = self.tempo_events[pos_index].duration;
                        if duration_remaining >= dur {
                            // Remove index
                            self.tempo_events.remove(pos_index);
                            duration_remaining -= dur;
                        } else {
                            // Shorten this value and break
                            self.tempo_events[pos_index].duration = dur - duration_remaining;
                            break;
                        }
                        if duration_remaining == Fraction::zero() {
                            break;
                        }
                        if pos_index == self.tempo_events.len() {
                            // We have removed all notes following the inserted one
                            break;
                        }
                    }
                }
            }
        } else {
            // The position is after the end of the MusicStream
            // Extend the MusicStream by that amount and insert the new value
            let extension_dur = position - self.total_tempo_event_duration();
            self.tempo_events
                .push(TempoEvent::no_change().dur(extension_dur));
            self.tempo_events.push(tempo_event);
        }

        self
    }
    pub fn total_tempo_event_duration(&self) -> Fraction {
        self.tempo_events
            .iter()
            .fold(Fraction::zero(), |sum, e| sum + e.duration)
    }
    pub fn total_bar_duration(&self) -> Fraction {
        let max_index = self.bars.len();
        self.get_total_position_at_bar_index(max_index)
    }
    pub fn get_bar_index_at_position(&self, position: Fraction) -> Option<usize> {
        let mut pos_pointer = Fraction::zero();
        for (i, bar) in self.bars.iter().enumerate() {
            let dur = bar.to_fraction();
            if position >= pos_pointer && position < pos_pointer + dur {
                return Some(i);
            }
            pos_pointer += dur;
        }
        None
    }
    pub fn insert_bar_at_position(&mut self, bar: Bar, position: Beat) {
        // First fill up with the last bar to be set until reaching the position.
        while position > self.total_bar_duration() {
            let last_bar = self.bars.last().unwrap().clone();
            self.bars.push(last_bar);
        }
        if position == self.total_bar_duration() {
            // Just insert the bar
            self.bars.push(bar);
        } else {
            let pos_index = self.get_bar_index_at_position(position);
            if let Some(mut pos_index) = pos_index {
                let index_pos = self.get_total_position_at_bar_index(pos_index);
                if index_pos < position {
                    // The `position` is in the middle of a bar.
                    // Shorten the bar at the position and jump forward one index
                    let new_duration = position - index_pos;
                    self.bars[pos_index].set_duration(new_duration);
                    pos_index += 1; // Insert after this pitch
                }
                // Save the duration of the event for the future
                let mut duration_remaining = bar.to_fraction();
                // Insert the new value
                self.bars.insert(pos_index, bar);

                // Check if there are any later values to shorten/remove
                if pos_index < self.bars.len() - 1 {
                    // Remove and potentially shorten values after this one

                    pos_index += 1; // Look at indices after the inserted note
                    loop {
                        let dur = self.bars[pos_index].to_fraction();
                        if duration_remaining >= dur {
                            // Remove index
                            self.bars.remove(pos_index);
                            duration_remaining -= dur;
                        } else {
                            // Shorten this value and break
                            self.bars[pos_index].set_duration(dur - duration_remaining);
                            break;
                        }
                        if duration_remaining == Fraction::zero() {
                            break;
                        }
                        if pos_index == self.bars.len() {
                            // We have removed all notes following the inserted one
                            break;
                        }
                    }
                }
            }
        }
    }
    pub fn get_bar_at_rhythmic_value(&self, position: Beat) -> Bar {
        if self.bars.is_empty() {
            eprintln!("This TemporalGrid has no bars, cannot give bar lengths");
            return Bar::zero();
        }
        if position < Fraction::from(0) {
            eprintln!("Trying to get bar from position before 0");
            return Bar::zero();
        }
        let mut pos_pointer = Fraction::zero();
        for b in &self.bars {
            let bar_dur = b.to_fraction();
            if position >= pos_pointer && position < pos_pointer + bar_dur {
                return b.clone();
            }
            pos_pointer += bar_dur;
        }
        // We ran out of bars before reaching the position. Therefore the bar length will be the last bar.
        // We have already checked that there are `Bar`s in self.bars above so this is safe
        self.bars.last().unwrap().clone()
    }

    pub fn get_bpm_at_rhythmic_value(&self, _position: Beat) -> Option<f32> {
        if self.bpm.is_some() {
            self.bpm
        } else {
            None
        }
    }

    pub fn to_time_signatures(&self, start: Fraction, end: Fraction) -> Vec<TimeSignature> {
        let mut time_signatures = Vec::new();
        let mut time_pointer = Fraction::zero();
        for bar in &self.bars {
            if time_pointer >= start && time_pointer < end {
                time_signatures.push(bar.to_time_signature());
            }
            time_pointer += bar.to_fraction();
        }
        if time_pointer <= end {
            let last_bar = self.bars.last().unwrap();
            while time_pointer <= end {
                time_signatures.push(last_bar.to_time_signature());
                time_pointer += last_bar.to_fraction();
            }
        }
        time_signatures
    }
    pub fn to_bars(&self, start: Fraction, end: Fraction) -> Vec<Bar> {
        let mut bars = Vec::new();
        let mut time_pointer = Fraction::zero();
        for bar in &self.bars {
            if time_pointer >= start && time_pointer < end {
                bars.push(bar.clone());
            }
            time_pointer += bar.to_fraction();
        }
        if time_pointer <= end {
            let last_bar = self.bars.last().unwrap();
            while time_pointer <= end {
                bars.push(last_bar.clone());
                time_pointer += last_bar.to_fraction();
            }
        }
        bars
    }
    pub fn get_position_in_piece(&self, position: Fraction) -> PositionInPiece {
        let mut pip = PositionInPiece::new();
        pip.raw_position = position;
        if position <= Fraction::zero() {
            // Positions in negative time return a PositionInPiece without bar information
            return pip;
        }
        let mut pos_pointer = Fraction::zero();
        for b in &self.bars {
            let bar_dur = b.to_fraction();
            if position >= pos_pointer && position < (pos_pointer + bar_dur) {
                pip.bar_position = position - pos_pointer;
                return pip;
            }
            pos_pointer += bar_dur;
            pip.bar_number += 1;
        }
        let last_bar = self.bars.last().unwrap();
        loop {
            let bar_dur = last_bar.to_fraction();
            if position >= pos_pointer && position < (pos_pointer + bar_dur) {
                pip.bar_position = position - pos_pointer;
                return pip;
            }
            pos_pointer += bar_dur;
            pip.bar_number += 1;
        }
    }
    /// Creates a tempo track with time signatures, metronome marks and tempo
    /// changes
    pub fn to_lilypond(&self, start: Fraction, end: Fraction) -> LPNode {
        let time_signatures = self.to_time_signatures(start, end);
        let mut ts_track = LPNode::sequence();
        let mut last_bar: Option<Bar> = None; // Keep track of bar to avoid duplicates
        let mut tempo_event_i = 0;
        let mut tempo_event_position = Fraction::zero();
        let mut current_position = start.clone();
        while tempo_event_i < self.tempo_events.len() && tempo_event_position < start {
            tempo_event_position += self.tempo_events[tempo_event_i].duration;
            tempo_event_i += 1;
        }
        let mut tempo_transition_ends = vec![];
        let mut next_rest_end: Option<&'static str> = None;
        for bar in self.to_bars(start, end) {
            // Bars!
            if let Some(last_bar_bar) = &last_bar {
                if bar != *last_bar_bar {
                    ts_track.push(bar.to_lilypond());
                    last_bar = Some(bar.clone());
                }
            } else {
                ts_track.push(bar.to_lilypond());
                last_bar = Some(bar.clone());
            }
            let mut bar_dur_left: Fraction = bar.to_fraction();
            // Check if we have any tempo_events to insert in this bar.
            //
            // Metronome marks only have to be inserted where they are using the
            // duration they have alternatively the rest of the bar.
            //
            // Tempo transitions have a start, which is the same as a metronome
            // mark, and an end which may be several bars in the future. The end
            // therefore has to be saved together with the remaining duration.
            // let tempo_events_this_bar =
            while tempo_event_i < self.tempo_events.len()
                && tempo_event_position < current_position + bar_dur_left
            {
                // A tempo_event should be placed in this bar
                let e: &TempoEvent = &self.tempo_events[tempo_event_i];
                // start_dur is the duration of the event that starts the tempo event
                let start_dur = if e.duration < bar_dur_left {
                    e.duration
                } else {
                    bar_dur_left
                };
                match &e.kind {
                    TempoEventKind::NoChange => {
                        let mut rest = LPNode::spacer_rest(start_dur);
                        // if let Some(end_cmd) = next_rest_end.take() {
                        //     rest.push_command(LPCommand::Other(end_cmd.to_owned()));
                        // }
                        ts_track.push(rest);
                        if let Some(end_cmd) = next_rest_end.take() {
                            ts_track.push(LPNode::other_cmd(end_cmd));
                        }
                    }
                    TempoEventKind::Fixed { bpm, text } => {
                        // TODO: Add text

                        // ts_track.push(LPNode::tempo_metronome_mark(4, bpm.round() as u32));
                        ts_track.push(LPNode::tempo(
                            Some(LPMetronomeMark::Fixed {
                                note_value: 4,
                                bpm: bpm.round() as u32,
                            }),
                            Some(text.clone()),
                            None,
                        ));
                        let mut rest = LPNode::spacer_rest(start_dur);
                        // The command should be attached to the rest instead of
                        // a separate command because the rest may be split into
                        // two.
                        // if let Some(end_cmd) = next_rest_end.take() {
                        //     rest.push_command(LPCommand::Other(end_cmd.to_owned()));
                        // }
                        ts_track.push(rest);
                        if let Some(end_cmd) = next_rest_end.take() {
                            ts_track.push(LPNode::other_cmd(end_cmd));
                        }
                    }
                    TempoEventKind::Transition {
                        start_bpm,
                        end_bpm,
                        curve,
                    } => {
                        let tempo_ratio = end_bpm / start_bpm;
                        let tempo_change_text = if tempo_ratio > 1.0 {
                            if tempo_ratio > 1.5 {
                                "molto accel."
                            } else if tempo_ratio < 1.1 {
                                "poco accel."
                            } else {
                                "accel."
                            }
                        } else {
                            if tempo_ratio < 0.5 {
                                "molto rit."
                            } else if tempo_ratio > 0.9 {
                                "poco rit."
                            } else {
                                "rit."
                            }
                        };
                        println!(
                            "Pushing tempo transition {} to rest with dur {}",
                            tempo_change_text, start_dur
                        );
                        let tempo_change_text =
                            format!("\\markup {{ \\upright \"{}\" }}", tempo_change_text);

                        ts_track.push(LPNode::override_cmd(
                            "TextSpanner.bound-details.left.text",
                            &tempo_change_text,
                        ));
                        let mut rest = LPNode::spacer_rest(start_dur);
                        // if let Some(end_cmd) = next_rest_end.take() {
                        //     rest.push_command(LPCommand::Other(end_cmd.to_owned()));
                        // }
                        ts_track.push(rest);
                        if let Some(end_cmd) = next_rest_end.take() {
                            ts_track.push(LPNode::other_cmd(end_cmd));
                        }
                        ts_track.push(LPNode::command(LPCommand::Other(
                            "\\startTextSpan".to_owned(),
                        )));

                        // This command should be added to the next spacer once the remaining duration has reached 0
                        // The duration of the note that was just added will be removed below
                        tempo_transition_ends.push(("\\stopTextSpan", e.duration));
                    }
                }
                bar_dur_left -= start_dur;
                current_position += start_dur;
                tempo_event_position += e.duration;
                tempo_event_i += 1;

                // Check if any tempo transition ends have been reached
                // If they have, store that end command and apply it to the next spacer rest
                for end in &mut tempo_transition_ends {
                    // Remove the duration of the rest that was just added above
                    end.1 -= start_dur;
                    if end.1 <= Fraction::zero() {
                        if next_rest_end.is_some() {
                            eprintln!("The previous next_rest_end was not used before finding a new one to be added.");
                        }
                        next_rest_end = Some(end.0);
                        if end.1 < Fraction::zero() {
                            eprintln!(
                                "End of tempo transition hit after the end of its duraiton: {}",
                                end.1
                            );
                        }
                    }
                }
                // Remove the expired tempo_transition_ends (should only ever be one at a time)
                tempo_transition_ends.retain(|end| end.1 > Fraction::zero());
            }
            while bar_dur_left > Fraction::zero() {
                // Find the next expiring tempo_transition_end
                let mut shortest_end_expiration = Fraction::max_value();
                let mut shortest_end_i = 0;
                for (i, end) in tempo_transition_ends.iter().enumerate() {
                    if end.1 > Fraction::zero() && end.1 < shortest_end_expiration {
                        shortest_end_expiration = end.1;
                        shortest_end_i = i;
                    }
                }
                let rest_dur = if shortest_end_expiration < bar_dur_left {
                    // Add a spacer rest to reach the position of the tempo_transition_end
                    shortest_end_expiration
                } else {
                    bar_dur_left
                };
                // Add spacer rests to fill the bar
                let mut rest = LPNode::spacer_rest(rest_dur);
                // if let Some(end_cmd) = next_rest_end.take() {
                //     rest.push_command(LPCommand::Other(end_cmd.to_owned()));
                // }
                ts_track.push(rest);
                if let Some(end_cmd) = next_rest_end.take() {
                    ts_track.push(LPNode::other_cmd(end_cmd));
                }
                bar_dur_left -= rest_dur;
                current_position += rest_dur;
                // Remove the dur of this rest from the tempo_transition_ends,
                // set any expired ones to be added to the next rest and remove
                // those from the list.
                for end in &mut tempo_transition_ends {
                    end.1 -= rest_dur;
                    if end.1 <= Fraction::zero() {
                        if next_rest_end.is_some() {
                            eprintln!("The previous next_rest_end was not used before finding a new one to be added.");
                        }
                        next_rest_end = Some(end.0);
                        if end.1 < Fraction::zero() {
                            eprintln!(
                                "End of tempo transition hit after the end of its duraiton: {}",
                                end.1
                            );
                        }
                    }
                }
                tempo_transition_ends.retain(|end| end.1 > Fraction::zero());
            }
        }
        // TODO: Logically, the offset to calculate_bar_positions should be
        // based on the start value
        ts_track.calculate_bar_positions(&time_signatures, PositionInPiece::new());
        // ts_track.split_compound_durations(&time_signatures);
        ts_track
    }
}

#[derive(Clone, Debug)]
pub struct TempoEvent {
    kind: TempoEventKind,
    pub duration: Fraction,
}
impl TempoEvent {
    pub fn no_change() -> Self {
        Self {
            kind: TempoEventKind::NoChange,
            duration: Fraction::zero(),
        }
    }
    pub fn bpm(bpm: f32, text: String) -> Self {
        Self {
            kind: TempoEventKind::Fixed { bpm, text },
            duration: Fraction::zero(),
        }
    }
    pub fn transition(from: f32, to: f32, curve: f32) -> Self {
        Self {
            kind: TempoEventKind::Transition {
                start_bpm: from,
                end_bpm: to,
                curve,
            },
            duration: Fraction::zero(),
        }
    }
    pub fn dur(mut self, duration: Fraction) -> Self {
        self.duration = duration;
        self
    }
    // Creates nodes to encode the tempo change in Lilypond, meant for a
    // separate tempo track. The second thing in the tuple is a command that
    // should be attached to the node immediately after the current one.
    //
    // TODO: This function makes no sense, since different TempoEventKinds need
    // such different treatment. Make a funciton on TemporalGrid which
    // generates a tempo track instead, taking bars and tempo changes into
    // account.
    // pub fn to_lilypond_vec(&self) -> (Vec<LPNode>, Option<String>, Option<String>) {
    //     match self.kind {
    //         TempoEventKind::NoChange => LPNode::nil(),
    //         TempoEventKind::Fixed { bpm, text } => {
    //             LPNode::tempo_metronome_mark(4, bpm.round() as u32)
    //         }
    //         TempoEventKind::Transition {
    //             start_bpm,
    //             end_bpm,
    //             curve,
    //         } => {
    //             let tempo_ratio = end_bpm / start_bpm;
    //             let tempo_change_text = if tempo_ratio > 1.0 {
    //                 if tempo_ratio > 1.5 {
    //                     "molto acc."
    //                 } else {
    //                     "acc"
    //                 }
    //             } else {
    //                 if tempo_ratio < 0.5 {
    //                     "molto rit."
    //                 } else {
    //                     "rit."
    //                 }
    //             };
    //             vec![
    //                 LPNode::override("TextSpanner.bound-details.left.text", ""),
    //                 LPNode::spacer_rest(duration).command("startTextSpan"),
    //                 // There could be a Fixed tempo event right after this one
    //                 // which in that case should have the endTextSpan command.
    //                 LPNode::spacer_rest(duration).command("endTextSpan"),
    //             ]
    //         }
    //     }
    // }
}
#[derive(Clone, Debug)]
pub enum TempoEventKind {
    NoChange, // To have a list of event with a duration, this has to exist like a rest between changes
    Fixed {
        bpm: f32,
        text: String,
    },
    Transition {
        start_bpm: f32,
        end_bpm: f32,
        curve: f32,
    },
}
/// A RhythmSequence is mapped onto bars by associating it with a TemporalGrid in a MusicStream
#[derive(Clone, Debug)]
pub struct RhythmSequence {
    beats: Vec<Beat>,
}

impl RhythmSequence {
    pub fn new(beats: Vec<Beat>) -> Self {
        Self { beats }
    }
    pub fn get(&self, i: usize) -> Beat {
        self.beats[i]
    }
    pub fn set(&mut self, i: usize, duration: Beat) {
        self.beats[i] = duration;
    }
    pub fn push(&mut self, duration: Beat) {
        self.beats.push(duration);
    }
    pub fn insert(&mut self, i: usize, duration: Beat) {
        self.beats.insert(i, duration);
    }
    pub fn len(&self) -> usize {
        self.beats.len()
    }
    pub fn last(&self) -> Beat {
        if !self.beats.is_empty() {
            self.beats[self.beats.len() - 1]
        } else {
            Fraction::zero()
        }
    }
    pub fn remove(&mut self, i: usize) {
        self.beats.remove(i);
    }
    pub fn get_slice(&self, start: usize, end: usize) -> &[Beat] {
        &self.beats[start..end]
    }
    pub fn iter(&self) -> std::slice::Iter<Beat> {
        self.beats.iter()
    }
    pub fn repeat(mut self, repeats: u32) -> Self {
        let num_elements = repeats * self.beats.len() as u32;
        self.beats = self
            .beats
            .into_iter()
            .cycle()
            .take(num_elements as usize)
            .collect();
        self
    }
}
