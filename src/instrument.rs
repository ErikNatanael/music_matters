use crate::Voice;
use lilypond_pargen::lp_node::*;

pub enum StaffType {
    Single,
    PianoStaff,
}

pub struct Instrument {
    pub name: String,
    pub staff_type: StaffType,
    pub voices: Vec<Voice>,
}

impl Instrument {
    pub fn marimba() -> Self {
        Self {
            name: "Marimba".to_owned(),
            staff_type: StaffType::PianoStaff,
            voices: vec![],
        }
    }

    pub fn to_lilypond_vec(&self) -> Vec<LPNode> {
        match self.staff_type {
            StaffType::PianoStaff => {
                // Envelop the voices in the appropriate Staff commands, for marimba:
                // \new PianoStaff \with {
                //   instrumentName = "Marimba"
                // } <<
                //   \new Staff = "right" \with {
                //     midiInstrument = "marimba"
                //   } \right
                //   \new Staff = "left" \with {
                //     midiInstrument = "marimba"
                //   } { \clef bass \left }
                // >>
                // TODO: Multiple voices per staff if there are more than two
                let (top_staff, bottom_staff) = if self.voices.len() == 1 {
                    // TODO: split voice into two
                    (self.voices[0].to_lilypond(), LPNode::sequence())
                } else if self.voices.len() == 2 {
                    (self.voices[0].to_lilypond(), self.voices[1].to_lilypond())
                } else {
                    (LPNode::sequence(), LPNode::sequence())
                };

                let new = LPNode::new_cmd("PianoStaff", None);
                let with =
                    LPNode::with_cmd(vec![LPProperty::instrument_name(self.name.clone())]);
                let parallel = LPNode::parallel_from(vec![
                    // right hand staff
                    LPNode::new_cmd("Staff", Some("right")),
                    LPNode::with_cmd(vec![LPProperty::midi_instrument("marimba")]),
                    // right hand staff music
                    top_staff,
                    // left hand staff
                    LPNode::new_cmd("Staff", Some("left")),
                    LPNode::with_cmd(vec![LPProperty::midi_instrument("marimba")]),
                    // left hand staff music
                    LPNode::sequence_from(vec![LPNode::clef("bass"), bottom_staff]),
                ]);

                vec![new, with, parallel]
            }
            StaffType::Single => {
                let new = LPNode::new_cmd("Staff".to_owned(), None);
                let with = LPNode::with_cmd(vec![LPProperty::instrument_name("Marimba")]);

                let mut parallel_voices = vec![];
                for voice in &self.voices {
                    parallel_voices.push(voice.to_lilypond());
                }

                vec![new, with, LPNode::parallel_from(parallel_voices)]
            }
        }
    }
}
