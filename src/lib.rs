pub mod harmonic_network;
pub mod note_names;

pub mod instrument;
pub mod pitch;
pub mod time;

use std::collections::HashMap;

use anyhow::{anyhow, Result};
use fraction::ToPrimitive;
use fraction::{Fraction, Zero};
use lilypond_pargen::lp_node::*;
use lilypond_pargen::parser::split_compound_duration_notes;
use pitch::*;

const C0: f32 = 130.8127; // This is the c with octave 0 in lilypond

/// Events use an API where self is usually taken and returned,
/// enabling succint creation and manipulation:
/// TODO: Example
///
/// A musical event that can represent a wide variety of musical
/// materials, from single notes and rest to chords and tremolos to
/// music boxes and other aleatoric notations. This means that an
/// event can contain other events (mostly note events).
///
/// If this was a trait, users of the library could define their own
/// event types with the tradeoff that collections of Event would have
/// to use trait objects combined with potential other objects for
/// exporting. This is more complicated than using trait methods on a
/// typed object.
#[derive(Clone, Debug)]
pub struct Event {
    pub kind: EventKind,
    pub duration: Fraction,
    /// List of `Dynamic`s attached to the event (use offset if more than one per event)
    pub dynamics: Vec<Dynamic>,
    pub texts: Vec<EventText>,
}

impl Default for Event {
    fn default() -> Self {
        Self {
            kind: EventKind::Rest,
            duration: Fraction::zero(),
            dynamics: vec![],
            texts: vec![],
        }
    }
}

impl Event {
    // INSTANCIATING FUNCTIONS
    pub fn note(pitch: Pitch) -> Self {
        Self {
            kind: EventKind::Note {
                pitch,
                note_kind: NoteKind::Default,
            },
            ..Default::default()
        }
    }
    pub fn rest() -> Self {
        Self {
            kind: EventKind::Rest,
            ..Default::default()
        }
    }
    /// Create unmeasures tremolo on one pitch
    pub fn single_tremolo(pitch: Pitch, subdivisions: u32) -> Self {
        Self {
            kind: EventKind::Aleatoric(AleatoricEvent::SingleTremolo {
                pitch,
                subdivisions,
            }),
            ..Default::default()
        }
    }
    /// Create unmeasures tremolo between two pitches
    pub fn double_tremolo(pitch1: Pitch, pitch2: Pitch, subdivisions: u32) -> Self {
        Self {
            kind: EventKind::Aleatoric(AleatoricEvent::DoubleTremolo {
                pitch1,
                pitch2,
                subdivisions,
            }),
            ..Default::default()
        }
    }
    // MODIFYING FUNCTIONS
    #[must_use]
    pub fn dur(mut self, duration: Fraction) -> Self {
        self.duration = duration;
        self
    }
    #[must_use]
    pub fn note_kind(mut self, new_note_kind: NoteKind) -> Self {
        if let EventKind::Note {
            pitch: _,
            note_kind,
        } = &mut self.kind
        {
            *note_kind = new_note_kind;
        }
        self
    }
    #[must_use]
    pub fn transpose(mut self, interval: Interval) -> Self {
        match &mut self.kind {
            EventKind::Note {
                pitch,
                note_kind: _,
            } => pitch.transpose(interval),
            EventKind::Rest => (),
            EventKind::Aleatoric(ae) => match ae {
                AleatoricEvent::DoubleTremolo {
                    pitch1,
                    pitch2,
                    subdivisions: _,
                } => {
                    pitch1.transpose(interval);
                    pitch2.transpose(interval);
                }
                AleatoricEvent::SingleTremolo {
                    pitch,
                    subdivisions: _,
                } => pitch.transpose(interval),
            },
        }
        self
    }
    #[must_use]
    pub fn text(mut self, text: EventText) -> Self {
        self.texts.push(text);
        self
    }
    #[must_use]
    pub fn dynamic(mut self, dynamic: Dynamic) -> Self {
        self.dynamics.push(dynamic);
        self
    }
    // GETTERS
    /// Returns a Pitch if this Event has one, otherwise None
    pub fn get_pitch(&self) -> Option<Pitch> {
        match &self.kind {
            EventKind::Note {
                pitch,
                note_kind: _,
            } => Some(*pitch),
            EventKind::Rest => None,
            EventKind::Aleatoric(ae) => match ae {
                AleatoricEvent::DoubleTremolo {
                    pitch1,
                    pitch2: _,
                    subdivisions: _,
                } => Some(*pitch1),
                AleatoricEvent::SingleTremolo {
                    pitch,
                    subdivisions: _,
                } => Some(*pitch),
            },
        }
    }
    // LILYPOND
    pub fn to_lilypond(&self) -> LPNode {
        match &self.kind {
            EventKind::Note {
                pitch,
                note_kind: note_type,
            } => {
                let mut node =
                    LPNode::note_from_midi_note(pitch_to_midi_note(pitch), self.duration);
                match note_type {
                    NoteKind::Default => (),
                    NoteKind::Action => node.set_note_head(LPNoteHead::Slash),
                    NoteKind::Hit => node.set_note_head(LPNoteHead::Cross),
                }
                for t in &self.texts {
                    node.push_markup(t.to_lilypond_markup());
                }
                for d in &self.dynamics {
                    d.add_to_lpnode(&mut node);
                }
                node
            }

            EventKind::Rest => {
                let mut node = if self.duration > Fraction::from(1.) {
                    LPNode::spacer_rest(self.duration)
                } else {
                    LPNode::rest(self.duration)
                };
                for t in &self.texts {
                    node.push_markup(t.to_lilypond_markup());
                }
                for d in &self.dynamics {
                    d.add_to_lpnode(&mut node);
                }
                node
            }
            EventKind::Aleatoric(ae) => match ae {
                AleatoricEvent::DoubleTremolo {
                    pitch1,
                    pitch2,
                    subdivisions,
                } => {
                    let note_duration = Fraction::new(1_u64, *subdivisions);
                    let node1 =
                        LPNode::note_from_midi_note(pitch_to_midi_note(pitch1), note_duration);
                    let node2 =
                        LPNode::note_from_midi_note(pitch_to_midi_note(pitch2), note_duration);
                    let mut node = LPNode::tremolo_double(node1, node2, self.duration);
                    for t in &self.texts {
                        node.push_markup(t.to_lilypond_markup());
                    }
                    for d in &self.dynamics {
                        d.add_to_lpnode(&mut node);
                    }
                    node
                }
                AleatoricEvent::SingleTremolo {
                    pitch,
                    subdivisions,
                } => {
                    let note_duration = self.duration / Fraction::from(*subdivisions);
                    let node =
                        LPNode::note_from_midi_note(pitch_to_midi_note(pitch), note_duration);
                    let mut node = LPNode::tremolo_single(node, self.duration);
                    for t in &self.texts {
                        node.push_markup(t.to_lilypond_markup());
                    }
                    for d in &self.dynamics {
                        d.add_to_lpnode(&mut node);
                    }
                    node
                }
            },
        }
    }
}

fn pitch_to_midi_note(p: &Pitch) -> f32 {
    let edo_pitch = p.to_edo(12, C0);
    // Add the edo_pitch as a midi_note number (degree + 48)
    edo_pitch.degree as f32 + 48.
}

#[derive(Clone, Debug)]
pub enum EventKind {
    Note { pitch: Pitch, note_kind: NoteKind },
    Rest,
    Aleatoric(AleatoricEvent),
}
#[derive(Clone, Debug)]
pub enum AleatoricEvent {
    DoubleTremolo {
        pitch1: Pitch,
        pitch2: Pitch,
        subdivisions: u32,
    },
    SingleTremolo {
        pitch: Pitch,
        subdivisions: u32,
    },
}

#[derive(Clone, Copy, Debug)]
pub enum NoteKind {
    Default,
    /// A custom action taken by the player
    Action,
    Hit,
}

#[derive(Clone, Debug)]
pub enum EventTextKind {
    Technique,
    Spoken,
}
#[derive(Clone, Debug)]
pub struct EventText {
    text: String,
    kind: EventTextKind,
}

impl EventText {
    pub fn technique(text: String) -> Self {
        Self {
            text,
            kind: EventTextKind::Technique,
        }
    }
    pub fn spoken(text: String) -> Self {
        Self {
            text,
            kind: EventTextKind::Spoken,
        }
    }
    pub fn to_lilypond_markup(&self) -> LPMarkup {
        let mut nodes = vec![];
        let mut direction = LPDirection::Default;
        match &self.kind {
            EventTextKind::Technique => {
                direction = LPDirection::Up;
                nodes.push(LPMarkupNode::text(self.text.clone()));
            }
            EventTextKind::Spoken => {
                direction = LPDirection::Up;
                nodes.push(LPMarkupNode::Italic);
                nodes.push(LPMarkupNode::text(self.text.clone()));
            }
        }
        LPMarkup::new(nodes, direction)
    }
}

/// A musical event can have a dynamic attached to its starting
/// position. However, dynamics may also occur in the middle of a long
/// note. A general treatment of dynamics would be to either separate
/// them from events, or to allow any number of dynamics within an
/// event with duration offsets. This offset, if tied to the event,
/// should be in a fraction of the event duration. This makes it
/// easier to play with the duration of the event and have dynamics
/// follow as intended.
///
/// The idea behind the level/sublevel encoding is to be more flexible
/// than standard notation, while still supporting it fully. There are
/// some ambiguous conversions, such as level 0. The sublevel can also
/// be used to encode more fluid/higher resolution dynamics for
/// electronic music notation and composition by being able to clearly
/// define the range of dynamics (level) and the play within that
/// dynamic (sublevel) as needed. In the case of electronic music
/// notation there is no need to stick to the default number of levels
/// (-3 to 3).
///
/// N.B.: `DynamicChange` is a different type for e.g. cresc. and dim.
#[derive(Clone, Debug)]
pub struct Dynamic {
    pub kind: DynamicKind,
    /// Offset from the start of the event as a fraction of that event's duration
    /// Has to be implemented using a parallel track with spacer rests in Lilypond
    pub offset_relative: Fraction,
}

impl Dynamic {
    pub fn from_text<T: AsRef<str>>(dynamic: T) -> Self {
        let (level, sublevel) = match dynamic.as_ref() {
            "ppp" => (-3, 0.0),
            "pp" => (-2, 0.0),
            "p" => (-1, 0.0),
            "mp" => (-1, 0.5),
            "mf" => (1, -0.5),
            "f" => (1, 0.0),
            "ff" => (2, 0.0),
            "fff" => (3, 0.0),
            _ => (0, 0.0),
        };
        let d = if (level, sublevel) != (0, 0.0) {
            Self {
                kind: DynamicKind::Fixed(DynamicFixed {
                    level,
                    sublevel,
                    subito: false,
                    relative: None,
                }),
                offset_relative: Fraction::zero(),
            }
        } else {
            match dynamic.as_ref() {
                "cresc" | "cresc." => Self {
                    kind: DynamicKind::Continuous(DynamicContinuous {
                        kind: DynamicContinuousKind::Crescendo,
                        hairpin: false,
                    }),
                    offset_relative: Fraction::zero(),
                },
                "<" => Self {
                    kind: DynamicKind::Continuous(DynamicContinuous {
                        kind: DynamicContinuousKind::Crescendo,
                        hairpin: true,
                    }),
                    offset_relative: Fraction::zero(),
                },
                "dim" | "dim." => Self {
                    kind: DynamicKind::Continuous(DynamicContinuous {
                        kind: DynamicContinuousKind::Diminuendo,
                        hairpin: false,
                    }),
                    offset_relative: Fraction::zero(),
                },
                ">" => Self {
                    kind: DynamicKind::Continuous(DynamicContinuous {
                        kind: DynamicContinuousKind::Diminuendo,
                        hairpin: true,
                    }),
                    offset_relative: Fraction::zero(),
                },
                "fp" => Self {
                    kind: DynamicKind::Special(DynamicSpecial::FP),
                    offset_relative: Fraction::zero(),
                },

                "!" => Self {
                    kind: DynamicKind::Continuous(DynamicContinuous {
                        kind: DynamicContinuousKind::End,
                        hairpin: false,
                    }),
                    offset_relative: Fraction::zero(),
                },
                _ => Self {
                    kind: DynamicKind::Special(DynamicSpecial::Free(dynamic.as_ref().to_owned())),
                    offset_relative: Fraction::zero(),
                },
            }
        };
        d
    }
    /// Some dynamics have to be added as markup objects in lilypond
    /// e.g. molto f, più p etc. Adding a dynamic in the middle of a
    /// note in Lilypond requires a whole extra parallel track of
    /// spacer rests to which the dynamics can be attached to
    pub fn add_to_lpnode(&self, node: &mut LPNode) {
        match &self.kind {
            DynamicKind::Fixed(df) => {
                let direction = LPDirection::Default;
                let mezzo =
                    (df.level < 0 && df.sublevel >= 0.5) || (df.level > 0 && df.sublevel <= -0.5);
                let mut dynamic_kind = match (df.level, mezzo) {
                    (-5, false) => LPDynamicKind::PPPPP,
                    (-4, false) => LPDynamicKind::PPPP,
                    (-3, false) => LPDynamicKind::PPP,
                    (-2, false) => LPDynamicKind::PP,
                    (-1, false) => LPDynamicKind::P,
                    (-1, true) => LPDynamicKind::MP,
                    (1, true) => LPDynamicKind::MF,
                    (1, false) => LPDynamicKind::F,
                    (2, false) => LPDynamicKind::FF,
                    (3, false) => LPDynamicKind::FFF,
                    (4, false) => LPDynamicKind::FFFF,
                    (5, false) => LPDynamicKind::FFFFF,
                    _ => LPDynamicKind::EndDynamic,
                };
                if df.subito {
                    if matches!(dynamic_kind, LPDynamicKind::P) {
                        dynamic_kind = LPDynamicKind::SP;
                        node.push_dynamic(LPDynamic {
                            dynamic_kind,
                            direction,
                        });
                    } else if matches!(dynamic_kind, LPDynamicKind::F) {
                        dynamic_kind = LPDynamicKind::SF;
                        node.push_dynamic(LPDynamic {
                            dynamic_kind,
                            direction,
                        });
                    } else {
                        // Use text object instead
                        // TODO: make-dynamic-script should ideally be used for
                        // placement in the center:
                        // https://lilypond.org/doc/v2.23/Documentation/notation-big-page.html#dynamics
                        let markup = LPMarkup::new(
                            vec![
                                LPMarkupNode::Text("sub.".to_owned()),
                                LPMarkupNode::Dynamic,
                                LPMarkupNode::Text(dynamic_kind.as_str().to_owned()),
                            ],
                            direction,
                        );
                        node.push_markup(markup);
                    }
                } else {
                    node.push_dynamic(LPDynamic {
                        dynamic_kind,
                        direction,
                    });
                }
            }
            DynamicKind::Continuous(dc) => {
                let dynamic_kind = match (dc.kind, dc.hairpin) {
                    (DynamicContinuousKind::Crescendo, true) => LPDynamicKind::CrescHairpin,
                    (DynamicContinuousKind::Crescendo, false) => LPDynamicKind::Cresc,
                    (DynamicContinuousKind::Diminuendo, true) => LPDynamicKind::DimHairpin,
                    (DynamicContinuousKind::Diminuendo, false) => LPDynamicKind::Dim,
                    (DynamicContinuousKind::End, _) => LPDynamicKind::EndDynamic,
                };
                let direction = LPDirection::Default;
                node.push_dynamic(LPDynamic {
                    dynamic_kind,
                    direction,
                });
            }
            DynamicKind::Special(ds) => {
                match ds {
                    DynamicSpecial::FP => {
                        let dynamic_kind = LPDynamicKind::FP;
                        let direction = LPDirection::Default;
                        node.push_dynamic(LPDynamic {
                            dynamic_kind,
                            direction,
                        });
                    }
                    DynamicSpecial::Free(text) => {
                        // TODO: Add the Dynamic markup node only before tokens which are dynamics
                        let texts = text.split(' ');
                        let mut markup_nodes = vec![LPMarkupNode::Dynamic];
                        for t in texts {
                            markup_nodes.push(LPMarkupNode::Text(t.to_string()));
                        }
                        let markup = LPMarkup::new(markup_nodes, LPDirection::Down);
                        node.push_markup(markup);
                    }
                };
            }
        }
    }
}
#[derive(Clone, Debug)]
pub enum DynamicKind {
    Fixed(DynamicFixed),
    Special(DynamicSpecial),
    Continuous(DynamicContinuous),
}
#[derive(Clone, Debug)]
pub struct DynamicFixed {
    /// Number of p or f, and a neutral level 0, or the general/main level of the dynamic
    pub level: i32,
    /// expected to be -1.0 < modifier > 1.0. mezzo is <=-0.5 for f and >=0.5 for p
    pub sublevel: f32,
    pub subito: bool,
    pub relative: Option<DynamicRelative>,
}

#[derive(Clone, Copy, Debug)]
pub struct DynamicContinuous {
    pub kind: DynamicContinuousKind,
    pub hairpin: bool,
}
#[derive(Clone, Debug)]
pub enum DynamicSpecial {
    FP,
    Free(String),
}
#[derive(Clone, Copy, Debug)]
pub enum DynamicContinuousKind {
    Crescendo,
    Diminuendo,
    End,
}

#[derive(Clone, Copy, Debug)]
pub enum DynamicRelative {
    More, // più
    Less, // meno
}

/// Options to use when exporting this MusicStream
/// In the future, this might hold specific general settings in addition to the
/// HashMap
#[derive(Clone, Debug)]
pub struct ExportOptions {
    options: HashMap<String, String>,
}
impl ExportOptions {
    pub fn new() -> Self {
        Self {
            options: HashMap::new(),
        }
    }
}

/// Match different kinds of musical material (pitches, articulation,
/// dynamics etc) to a RhythmSequence. The length is that of the
/// longest sequence. If the sequences have different lengths the
/// shorter sequences repeat their last values until all sequences are
/// depleted.
#[derive(Clone, Debug)]
pub struct MusicStream {
    events: Vec<Event>,
    /// A time offset allowing you to have multiple MusicStreams in
    /// parallel or in sequence. TODO: Should this be handled more
    /// explicitly so that a separate stream can include multiple
    /// MusicStreams and then that stream is what Voice uses so that
    /// all of those streams in a Voice are in sequence?
    pub offset: time::Beat,
    pub export_options: ExportOptions,
}

impl MusicStream {
    pub fn new() -> Self {
        Self {
            events: vec![],
            offset: time::Beat::zero(),
            export_options: ExportOptions::new(),
        }
    }
    pub fn from_events(events: Vec<Event>) -> Self {
        Self {
            events,
            offset: Fraction::zero(),
            export_options: ExportOptions::new(),
        }
    }
    pub fn from_rhythm_and_pitch_seqs(
        mut rhythm_seq: time::RhythmSequence,
        mut pitch_seq: pitch::PitchSequence,
    ) -> Self {
        // Extend the sequences if necessary
        if rhythm_seq.len() < pitch_seq.len() {
            let last = rhythm_seq.last();
            let num = pitch_seq.len() - rhythm_seq.len();
            for _i in 0..num {
                rhythm_seq.push(last);
            }
        } else if rhythm_seq.len() > pitch_seq.len() {
            let last = pitch_seq.last();
            let num = rhythm_seq.len() - pitch_seq.len();
            for _i in 0..num {
                pitch_seq.push(last);
            }
        }
        let events = rhythm_seq
            .iter()
            .zip(pitch_seq.get_pitches().iter())
            .map(|(dur, pitch)| match pitch {
                Some(pitch) => Event::note(*pitch).dur(*dur),
                None => Event::rest().dur(*dur),
            })
            .collect();
        let mut s = Self {
            events,
            offset: time::Beat::zero(),
            export_options: ExportOptions::new(),
        };

        s
    }
    pub fn add_export_option<T: AsRef<str>>(&mut self, parameter: T, argument: T) {
        self.export_options
            .options
            .insert(parameter.as_ref().to_owned(), argument.as_ref().to_owned());
    }
    /// Returns the duration and any other parameters that the
    /// MusicStream includes for a given index.
    pub fn get_at_index(&self, index: usize) -> Result<&Event, &str> {
        if index >= self.events.len() {
            return Err("index higher than the longest sequence in the MusicStream");
        }

        Ok(&self.events[index])
    }
    pub fn get_mut_at_index(&mut self, index: usize) -> Result<&mut Event, &str> {
        if index >= self.events.len() {
            return Err("index higher than the longest sequence in the MusicStream");
        }

        Ok(&mut self.events[index])
    }
    /// Get the position of a certain index including the offset. This
    /// is useful for comparing the position of items in different MusicStreams
    pub fn get_total_position_at_index(&self, index: usize) -> time::Beat {
        if index <= self.events.len() {
            self.events[0..index]
                .iter()
                .fold(Fraction::zero(), |sum, event| sum + event.duration)
                + self.offset
        } else {
            panic!("Trying to access MusicStream index out of bounds!");
        }
    }
    /// Get the total duration of this MusicStream
    pub fn total_duration(&self) -> Fraction {
        let max_index = self.events.len();
        self.get_total_position_at_index(max_index)
    }
    pub fn map_event(&mut self, index: usize, mut c: impl FnOnce(Event) -> Event) {
        if index < self.events.len() {
            let e = self.events[index].clone();
            let e = c(e);
            self.events[index] = e;
        } else {
            let event_res = c(Event::rest());
            panic!(
                "map_event: index out of bounds: {}/{}\nCallback result: {:?}",
                index,
                self.events.len(),
                event_res
            )
        }
    }
    /// Returns two MusicStreams, (top, bottom) where top includes
    /// pitches that are higher than or equal to the `split_pitch`,
    /// bottom includes all other pitches. Rhythms and any other
    /// sequences are duplicated. More complex events are split on a
    /// best effort basis.
    pub fn split_at_pitch(self, split_pitch: Pitch) -> (Self, Self) {
        let mut top_events = vec![];
        let mut bottom_events = vec![];
        for event in self.events.iter() {
            if let Some(p) = event.get_pitch() {
                if p >= split_pitch {
                    top_events.push(event.clone());
                    bottom_events.push(Event::rest().dur(event.duration));
                } else {
                    top_events.push(Event::rest().dur(event.duration));
                    bottom_events.push(event.clone());
                }
            } else {
                top_events.push(event.clone());
                bottom_events.push(event.clone());
            }
        }
        (
            MusicStream::from_events(top_events),
            MusicStream::from_events(bottom_events),
        )
    }
    pub fn insert_at_position(&mut self, event: Event, position: Fraction) -> &mut Self {
        // If the position is exactly at a duration, insert the new
        // values at that index and then remove `duration` worth of
        // values afterwards.

        if position < self.total_duration() {
            let pos_index = self.get_index_at_position(position);
            if let Some(mut pos_index) = pos_index {
                let index_pos = self.get_total_position_at_index(pos_index);
                if index_pos < position {
                    // The `position` is in the middle of a note.
                    // Shorten the value at the position and jump forward one index
                    let new_duration = position - index_pos;
                    self.events[pos_index].duration = new_duration;
                    pos_index += 1; // Insert after this pitch
                }
                // Save the duration of the event for the future
                let mut duration_remaining = event.duration;
                // Insert the new value
                self.events.insert(pos_index, event);

                // Check if there are any later values to shorten/remove
                if pos_index < self.events.len() - 1 {
                    // Remove and potentially shorten values after this one

                    pos_index += 1; // Look at indices after the inserted note
                    loop {
                        let dur = self.events[pos_index].duration;
                        if duration_remaining >= dur {
                            // Remove index
                            self.events.remove(pos_index);
                            duration_remaining -= dur;
                        } else {
                            // Shorten this value and break
                            self.events[pos_index].duration = dur - duration_remaining;
                            break;
                        }
                        if duration_remaining == Fraction::zero() {
                            break;
                        }
                        if pos_index == self.events.len() {
                            // We have removed all notes following the inserted one
                            break;
                        }
                    }
                }
            }
        } else {
            // The position is after the end of the MusicStream
            // Extend the MusicStream by that amount and insert the new value
            let extension_dur = position - self.total_duration();
            self.events.push(Event::rest().dur(extension_dur));
            self.events.push(event);
        }

        self
    }
    pub fn merge_rests(&mut self) {
        let mut new_events = vec![];
        let mut accumulated_rest: Option<Event> = None;
        for e in &self.events {
            if matches!(e.kind, EventKind::Rest) {
                let mut replace_rest = false;
                if let Some(a_rest) = &mut accumulated_rest {
                    // We don't want to merge rests which have dynamics or text attached to them
                    if e.dynamics.is_empty() && e.texts.is_empty() {
                        a_rest.duration += e.duration;
                    } else {
                        replace_rest = true;
                    }
                } else {
                    replace_rest = true;
                }
                if replace_rest {
                    let a_rest = accumulated_rest.take();
                    if let Some(rest) = a_rest {
                        new_events.push(rest);
                    }
                    accumulated_rest = Some(e.clone());
                }
            } else {
                // Push the accumulated rest if there is one
                let rest = accumulated_rest.take();
                if let Some(rest) = rest {
                    new_events.push(rest);
                }
                // Push the non rest event:
                new_events.push(e.clone());
            }
        }
        self.events = new_events;
    }
    pub fn get_index_at_position(&self, position: Fraction) -> Option<usize> {
        let mut pos_pointer = Fraction::zero();
        for (i, event) in self.events.iter().enumerate() {
            if position >= pos_pointer && position < pos_pointer + event.duration {
                return Some(i);
            }
            pos_pointer += event.duration;
        }
        None
    }
    pub fn get_at_position(&self, position: Fraction) -> Option<Event> {
        let mut pos_pointer = Fraction::zero();
        for (i, event) in self.events.iter().enumerate() {
            if position >= pos_pointer && position < pos_pointer + event.duration {
                return Some(event.clone());
            }
            pos_pointer += event.duration;
        }
        None
    }
    /// Splits the events in the MusicStream so that they fit within the bars.
    /// This is necessary e.g. to insert time signature commands when exporting
    /// to Lilypond
    pub fn split_events_into_bars(mut self, bars: &[time::Bar]) -> Self {
        let mut pip = PositionInPiece::new();
        // First go to the offset position

        self
    }
    pub fn len(&self) -> usize {
        self.events.len()
    }
    pub fn push_event(&mut self, event: Event) -> &mut Self {
        self.events.push(event);
        self
    }
    /// Adds all of the events of the stream to the end of self, ignoring the offset
    pub fn push_stream(&mut self, stream: MusicStream) -> &mut Self {
        self.events.extend(stream.events.into_iter());
        self
    }
    /// Transpose the PitchSequence of the stream if there is one
    pub fn transpose(mut self, interval: Interval) -> Self {
        self.events = self
            .events
            .into_iter()
            .map(|event| event.transpose(interval))
            .collect();
        self
    }
    pub fn repeat(mut self, repeats: u32) -> Self {
        let mut new_events = self.events.clone();
        for _i in 0..repeats {
            new_events.extend_from_slice(&self.events[..]);
        }
        self.events = new_events;
        self
    }
    /// Changes the duration of the MusicStream by adding rests to the end or removing events from the end.
    /// This is useful to make two `MusicStream`s the same duration.
    pub fn into_duration(mut self, duration: Fraction) -> Self {
        let mut own_duration = self.total_duration();
        if own_duration == duration {
            return self;
        } else if own_duration > duration {
            // Remove events from the end
            while own_duration > duration && !self.events.is_empty() {
                // Can we remove the whole event or must we shorten it?
                // Unwraps safe because we already checked that there are items in the Vec
                let last_event_dur = self.events.last().unwrap().duration;
                if own_duration - last_event_dur >= duration {
                    let removed = self.events.pop().unwrap();
                    own_duration -= removed.duration;
                } else {
                    // Shorten the last event to the remaining dur
                    let remaining_diff = own_duration - duration;
                    self.events.last_mut().unwrap().duration = last_event_dur - remaining_diff;
                }
            }
        } else if own_duration < duration {
            // Add a long rest
            self.push_event(Event::rest().dur(duration - own_duration));
        }
        self
    }
}

#[derive(Clone, Debug)]
pub struct Voice {
    pub music_streams: Vec<MusicStream>,
    pub temporal_grid: time::TemporalGrid,
}

impl Voice {
    pub fn new(music_streams: Vec<MusicStream>, temporal_grid: &time::TemporalGrid) -> Self {
        Self {
            music_streams,
            temporal_grid: temporal_grid.clone(),
        }
    }
    pub fn push_music_stream(&mut self, music_stream: MusicStream) {
        self.music_streams.push(music_stream);
    }
    pub fn insert_music_stream_after(&mut self, mut music_stream: MusicStream, index: usize) {
        let after_stream_offset =
            self.music_streams[index].total_duration() + self.music_streams[index].offset;
        println!("after_stream_offset: {}", after_stream_offset);
        music_stream.offset = after_stream_offset;
        self.music_streams.push(music_stream);
    }
    pub fn insert_music_stream_last(&mut self, music_stream: MusicStream) {
        // Find the music stream with the latest end point
        let mut latest_endpoint = Fraction::zero();
        let mut latest_index = 0;
        for (i, ms) in self.music_streams.iter().enumerate() {
            // total_duration() already includes the offset
            let endpoint = ms.total_duration();
            if endpoint > latest_endpoint {
                latest_endpoint = endpoint;
                latest_index = i;
            }
        }
        self.insert_music_stream_after(music_stream, latest_index);
    }
    /// Get the earliest offset `MusicStream` in the `Voice`. If several `MusicStream`s have the same start point the index of the first to be found is returned.
    pub fn get_start_point(&self) -> (Fraction, usize) {
        use fraction::Bounded;
        let mut earliest_startpoint = Fraction::max_value();
        let mut earliest_index = 0;

        for (i, ms) in self.music_streams.iter().enumerate() {
            let startpoint = ms.offset;
            if startpoint < earliest_startpoint {
                earliest_startpoint = startpoint;
                earliest_index = i;
            }
        }
        (earliest_startpoint, earliest_index)
    }
    /// Get the longest duration + offset `MusicStream` in the `Voice`. If several `MusicStream`s have the same end point the index of the first to be found is returned.
    pub fn get_end_point(&self) -> (Fraction, usize) {
        let mut latest_endpoint: fraction::GenericFraction<u64> = Fraction::zero();
        let mut latest_index = 0;
        for (i, ms) in self.music_streams.iter().enumerate() {
            // total_duration() already includes the offset
            let endpoint = ms.total_duration();
            println!(
                "endpoint: {}, latest_endpoint: {}, larger: {:?}",
                endpoint.to_f32().unwrap(),
                latest_endpoint.to_f32().unwrap(),
                endpoint > latest_endpoint
            );
            if endpoint > latest_endpoint {
                latest_endpoint = endpoint;
                latest_index = i;
            }
        }
        (latest_endpoint, latest_index)
    }
    /// Splits the `MusicStream`s within at a certain pitch
    pub fn split_at_pitch(mut self, split_pitch: Pitch) -> (Self, Self) {
        let num_music_streams = self.music_streams.len();
        let music_streams = std::mem::replace(
            &mut self.music_streams,
            Vec::with_capacity(num_music_streams),
        );
        let mut top = self.clone();
        let mut bottom = self;
        for ms in music_streams {
            let (ts, bs) = ms.split_at_pitch(split_pitch);
            top.push_music_stream(ts);
            bottom.push_music_stream(bs);
        }

        (top, bottom)
    }
    pub fn to_lilypond(&self) -> LPNode {
        // TODO: depending on the layout of the MusicStreams there could be a need for lilypond nodes in parallel or in sequence
        let mut voice_parallel_node = LPNode::parallel();

        // let (start, _) = self.get_start_point();
        let start = Fraction::zero(); // Add spacer rests for offset instead
        let (end, _) = self.get_end_point();
        let time_signatures = self.temporal_grid.to_time_signatures(start, end);
        // let offset_position = self.temporal_grid.get_position_in_piece(start);

        // Convert all the music_streams to LPNodes. In order to do
        // this the different music streams must be
        // synchronised. Lilypond focuses on notes and rests so create
        // one parallel music expression for each music_stream
        // containing pitches. Then crossreference all the
        // music_streams for this Voice to apply articulation,
        // dynamics etc to the notes being generated.

        // Special cases:
        // * MusicStreams in parallel with the same rhythmic values: combine into one Voice
        // "\new Voice <<
        //   \relative { e''4 f8 d e16 f g8 d4 }
        //   \relative { c''4 d8 b c16 d e8 b4 }
        // >>"
        // * MusicStreams in parallel with different rhythms: separate voices
        // "\relative { r8 g'' g  g g f16 ees f8 d }
        // \\
        // \relative { ees'8 r ees r d r d r }
        // \\
        // \relative { d''8 s c s bes s a s }"
        // * Music Streams in sequence: place in without padding with invisible rests
        //

        // First create a time signature track. This saves us from setting the
        // time signature in every MusicStream and makes it easier to split
        // notes correctly, since they don't have to be presplit to accomodate
        // the bars.

        println!("Voice to lilypond");
        println!(
            "end: {:.3}, temporal_grid_bar_dur: {:.3}\ntemporal_grid_event_dur: {:.3}",
            end,
            self.temporal_grid.total_bar_duration(),
            self.temporal_grid.total_tempo_event_duration()
        );
        let ts_track = self.temporal_grid.to_lilypond(start, end);
        voice_parallel_node.push(ts_track);
        // println!("ts_track: {:#?}\n", ts_track);

        for (num, stream) in self.music_streams.iter().enumerate() {
            let mut current_clef = if let Some(c) = stream.export_options.options.get("start_clef")
            {
                Clef::new(&c.clone())
            } else {
                Clef::new("treble")
            };
            let do_change_clef = if let Some(val) = stream.export_options.options.get("clef_change")
            {
                if val == "true" {
                    true
                } else {
                    false
                }
            } else {
                false
            };
            // Create a sequence node per music stream with pitches
            let mut sequence_node = LPNode::sequence();
            // voice_parallel_node.push(LPNode::new_cmd("Voice", None));
            // let stream = &self.music_streams[i];
            let num_items = stream.events.len();
            // Add rests for the offset from the current parallel node start
            // (for now all streams start from the beginning of the piece)
            if stream.offset > Fraction::zero() {
                let spacer_rest = LPNode::spacer_rest(stream.offset);
                sequence_node.push(spacer_rest);
                // Split compound duration later on
                // let spacer_rests =
                //     split_compound_duration_notes(&vec![spacer_rest], &TimeSignature::from(4, 4));
                // sequence_node.push_all(spacer_rests);
            }
            for node_i in 0..num_items {
                // Create an LPNode note from event
                let event = stream.get_at_index(node_i).unwrap();
                // Check first if we need to change clef
                if let Some(pitch) = event.get_pitch() {
                    let edo_pitch = pitch.to_edo(12, C0);
                    if do_change_clef {
                        if let Some(new_clef) = current_clef.should_change_clef(edo_pitch) {
                            current_clef.set(new_clef);
                            sequence_node.push(current_clef.to_lilypond());
                        }
                    }
                }
                // Now insert the event (note, rest etc) itself
                let node = event.to_lilypond();
                sequence_node.push(node);
            }
            voice_parallel_node.push(sequence_node);
            // Don't insert the mark for parallel lilypond voices after the last stream
            if num < self.music_streams.len() - 1 {
                voice_parallel_node.push(LPNode::other_cmd("\\\\"));
            }
        }
        // Perhaps there should be no offset_position since this offset is
        // filled in by spacer rests?
        let offset_position = PositionInPiece::new();
        println!("voice_parallel_node:");
        voice_parallel_node.calculate_bar_positions(&time_signatures, offset_position);
        voice_parallel_node.split_compound_durations(&time_signatures);

        voice_parallel_node
    }
}

/// Helper to figure out the optimal clef changes automatically
/// TODO: Make what clefs are available and in what registers customizable per instrument
pub struct Clef {
    /// The limit is (min, max) 12edo pitches (0.0 == c, 12.0 == c')
    limits: HashMap<String, (f32, f32)>,
    current_clef: String,
}
impl Clef {
    pub fn new<T: AsRef<str>>(clef: T) -> Self {
        let clef = clef.as_ref().to_owned();
        let limits: HashMap<String, (f32, f32)> = [
            ("treble".to_owned(), (7.0, 60.0)),
            ("bass".to_owned(), (-60.0, 14.0)),
        ]
        .iter()
        .cloned()
        .collect();
        if limits.get(&clef).is_none() {
            eprintln!("Create an invalid clef: {}", clef);
        }
        Self {
            limits,
            current_clef: clef,
        }
    }

    pub fn set<T: AsRef<str>>(&mut self, clef: T) {
        let clef = clef.as_ref().to_owned();
        if self.limits.get(&clef).is_none() {
            eprintln!("Set an invalid clef: {}", clef);
        }
        self.current_clef = clef;
    }

    pub fn should_change_clef(&self, edo_pitch: EdoPitch) -> Option<String> {
        let degree = edo_pitch.to_linear_ratio() * 12.0;
        if let Some(current_lim) = self.limits.get(&self.current_clef) {
            if degree < current_lim.0 || degree > current_lim.1 {
                // Find the best clef for this degree
                let mut distance_from_end = 0.; // How close to the limit of a clef the degree is
                let mut best_clef_name = String::new();
                for (clef_name, limit) in &self.limits {
                    if degree >= limit.0 && degree <= limit.1 {
                        let dist = (degree - limit.0).min(limit.1 - degree);
                        if dist > distance_from_end {
                            distance_from_end = dist;
                            best_clef_name = clef_name.clone();
                        }
                    }
                }
                if !best_clef_name.is_empty() {
                    Some(best_clef_name)
                } else {
                    // No suitable clef was found, don't change
                    None
                }
            } else {
                // No need to change, the current clef is fine
                None
            }
        } else {
            // The current_clef is not supported, do nothing
            None
        }
    }

    pub fn to_lilypond(&self) -> LPNode {
        LPNode::clef(&self.current_clef)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn harmonic_network() {
        // assert_eq!(2 + 2, 4);
        use crate::harmonic_network::HarmonicNetwork;
        use crate::note_names::NoteNames;
        use crate::pitch::*;

        let note_names = NoteNames::new();
        let mut harmonic_network = HarmonicNetwork::new(EdoPitch::new(0, 53), 3);
        // println!("{harmonic_network:#?}");
        for i in 0..100 {
            let new_chord = harmonic_network.get_next_chord();
            let mode_text = if new_chord.is_major() {
                "major"
            } else {
                "minor"
            };
            let note_text = note_names
                .edo_53_degree_to_name(new_chord.root.degree)
                .unwrap();
            println!("{note_text} {mode_text}");
        }
        assert!(true);
    }
}
