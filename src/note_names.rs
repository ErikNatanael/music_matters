use ahash::{AHasher, RandomState};
use phf::phf_map;
use std::collections::HashMap;

pub static EDO12INTERVALS: phf::Map<&'static str, i32> = phf_map! {
    "D1" => -1,
    "P1" => 0,
    "A1" => 1,
    "m2" => 1,
    "M2" => 2,
    "m3" => 3,
    "M3" => 4,
    "D4" => 4,
    "P4" => 5,
    "A4" => 6,
    "D5" => 6,
    "P5" => 7,
    "A5" => 8,
    "m6" => 8,
    "M6" => 9,
    "m7" => 10,
    "M7" => 11,
};

pub struct NoteNames {
    edo_53_name_to_degree: HashMap<String, i32, RandomState>,
    edo_53_degree_to_name: HashMap<i32, String, RandomState>,
    edo_53_chords_to_degrees: HashMap<String, Vec<i32>, RandomState>,
    edo_53_interval_degree_to_name: HashMap<i32, String, RandomState>,
}

impl NoteNames {
    pub fn new() -> Self {
        let mut edo_53_name_to_degree: HashMap<String, i32, RandomState> = HashMap::default();
        edo_53_name_to_degree.insert("C".into(), 0);
        edo_53_name_to_degree.insert("D".into(), 9);
        edo_53_name_to_degree.insert("E".into(), 18);
        edo_53_name_to_degree.insert("F".into(), 22);
        edo_53_name_to_degree.insert("G".into(), 31);
        edo_53_name_to_degree.insert("A".into(), 40);
        edo_53_name_to_degree.insert("B".into(), 49);
        let mut sharps_and_flats: HashMap<String, i32, RandomState> = HashMap::default();
        // Add sharps and flats
        for (key, value) in edo_53_name_to_degree.iter() {
            sharps_and_flats.insert(format!("{}#", key), value + 5);
            sharps_and_flats.insert(format!("{}b", key), value - 5);
        }
        for (key, value) in sharps_and_flats {
            edo_53_name_to_degree.insert(key, value);
        }
        // Add ups and downs
        let mut ups_and_downs: HashMap<String, i32, RandomState> = HashMap::default();
        for (key, value) in edo_53_name_to_degree.iter() {
            ups_and_downs.insert(format!("v{}", key), value - 1);
            ups_and_downs.insert(format!("vv{}", key), value - 2);
            ups_and_downs.insert(format!("^{}", key), value + 1);
            ups_and_downs.insert(format!("^^{}", key), value + 2);
        }
        for (key, value) in ups_and_downs {
            edo_53_name_to_degree.insert(key, value);
        }
        let mut edo_53_degree_to_name: HashMap<i32, String, RandomState> = HashMap::default();
        for i in 0..53 {
            let mut shortest_name: Option<String> = None;
            for (name, degree) in edo_53_name_to_degree.iter() {
                if *degree == i {
                    let shortest_taken = shortest_name.take();
                    if let Some(shortest) = shortest_taken {
                        if shortest.len() > name.len() {
                            shortest_name = Some(name.clone());
                        } else {
                            shortest_name = Some(shortest);
                        }
                    } else {
                        shortest_name = Some(name.clone());
                    }
                }
            }
            if let Some(name) = shortest_name {
                edo_53_degree_to_name.insert(i, name);
            }
        }

        // INTERVAL NAMES
        let mut edo_53_interval_degree_to_name: HashMap<i32, String, RandomState> = [
            (0, "P1"),
            (1, "^1"),
            (2, "^^1"),
            (3, "vvm2"),
            (4, "vm2"),
            (5, "m2"),
            (6, "v~2"),
            (7, "^~2"),
            (8, "vM2"),
            (9, "M2"),
            (10, "^M2"),
            (11, "vvm3"),
            (12, "vm3"),
            (13, "m3"),
            (14, "^m3"),
            (15, "v~3"),
            (16, "^~3"),
            (17, "vM3"),
            (18, "M3"),
            (19, "^M3"),
            (20, "^^M3"),
            (21, "v4"),
            (22, "P4"),
            (23, "^4"),
            (24, "v~4"),
            (25, "^~4"),
            (26, "vA4"),
            (27, "A4"),
            (28, "^A4"),
            (29, "vv5"),
            (30, "v5"),
            (31, "P5"),
            (32, "^5"),
            (33, "vvm6"),
            (34, "vm6"),
            (35, "m6"),
            (36, "^m6"),
            (37, "v~6"),
            (38, "^~6"),
            (39, "vM6"),
            (40, "M6"),
            (41, "^M6"),
            (42, "vvm7"),
            (43, "vm7"),
            (44, "m7"),
            (45, "^m7"),
            (46, "v~7"),
            (47, "^~7"),
            (48, "vM7"),
            (49, "M7"),
            (50, "^M7"),
            (51, "^^M7"),
            (52, "v8"),
            (53, "P8"),
        ]
        .into_iter()
        .map(|(degree, name)| (*degree as i32, String::from(*name)))
        .collect();

        // Add another octave of intervals by taking the same name and add 7 to the interval number
        let added_intervals: Vec<(i32, String)> = edo_53_interval_degree_to_name
            .iter()
            .map(|(degree, name)| {
                let mut new_name = name.clone();
                if let Some(num) = new_name.pop() {
                    if let Ok(num) = num.to_string().parse::<i32>() {
                        new_name.push_str(&format!("{}", num + 7));
                    } else {
                        panic!(format!(
                            "Failed to parse interval number: {}, degree: {}",
                            num, degree
                        ));
                    }
                } else {
                    panic!(format!("Interval name for degree {} was empty!", *degree));
                }
                (*degree + 53, new_name)
            })
            .collect();

        for (degree, name) in added_intervals {
            edo_53_interval_degree_to_name.insert(degree, name);
        }

        // CHORDS
        let mut edo_53_chords_to_degrees: HashMap<String, Vec<i32>, RandomState> =
            HashMap::default();
        edo_53_chords_to_degrees.insert("".into(), vec![0, 17, 31]);
        edo_53_chords_to_degrees.insert("m".into(), vec![0, 14, 31]);
        edo_53_chords_to_degrees.insert("m7".into(), vec![0, 14, 31, 45]);
        edo_53_chords_to_degrees.insert("7".into(), vec![0, 17, 31, 44]);
        edo_53_chords_to_degrees.insert("v7".into(), vec![0, 17, 31, 43]);
        edo_53_chords_to_degrees.insert("9v7".into(), vec![0, 9, 17, 31, 43]);
        edo_53_chords_to_degrees.insert("9".into(), vec![0, 9, 17, 31, 44]);
        edo_53_chords_to_degrees.insert("sus4".into(), vec![0, 22, 31]);
        edo_53_chords_to_degrees.insert("7sus4".into(), vec![0, 22, 31, 44]);
        edo_53_chords_to_degrees.insert("7sus2".into(), vec![0, 9, 31, 44]);
        edo_53_chords_to_degrees.insert("sus2".into(), vec![0, 9, 31]);
        Self {
            edo_53_name_to_degree,
            edo_53_degree_to_name,
            edo_53_chords_to_degrees,
            edo_53_interval_degree_to_name,
        }
    }
    pub fn edo_53_name_to_degree<S: AsRef<str>>(&self, name: S) -> Option<i32> {
        if let Some(degree) = self.edo_53_name_to_degree.get(name.as_ref()) {
            Some(*degree)
        } else {
            None
        }
    }
    pub fn edo_53_degree_to_name(&self, mut degree: i32) -> Option<String> {
        while degree < 0 {
            degree += 53;
        }
        degree = degree % 53;
        if let Some(name) = self.edo_53_degree_to_name.get(&degree) {
            Some(name.clone())
        } else {
            None
        }
    }
    pub fn edo_53_interval_degree_to_name(&self, mut degree: i32) -> Option<String> {
        let is_negative = degree.is_negative();
        degree = degree.abs();
        while degree >= 106 {
            degree -= 53;
        }
        if let Some(name) = self.edo_53_interval_degree_to_name.get(&degree) {
            if !is_negative {
                Some(name.clone())
            } else {
                let mut name = name.clone();
                name.insert(0, '-');
                Some(name)
            }
        } else {
            None
        }
    }
    pub fn get_edo_53_chords(&self) -> Vec<(String, Vec<i32>)> {
        let mut chords = Vec::new();
        for (name, degrees) in self.edo_53_chords_to_degrees.iter() {
            chords.push((name.clone(), degrees.clone()));
        }
        chords
    }
}
